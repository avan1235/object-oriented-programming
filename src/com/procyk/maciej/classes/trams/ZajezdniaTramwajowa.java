package com.procyk.maciej.classes.trams;

public class ZajezdniaTramwajowa extends Zajezdnia {

    public ZajezdniaTramwajowa(String nazwa) {
        super(nazwa);
    }

    @Override
    public void dodajPojazd(PojazdKomunikacjiMiejskiej p) {
        if (p instanceof Tramwaj)
            super.dodajPojazd(p);
        else
            throw new IllegalArgumentException("Nie można dodać " + p.getClass().getName() + " do " + this.getClass().getName());
    }
}
