package com.procyk.maciej.classes.trams;

public class Autobus extends PojazdKomunikacjiMiejskiej {

    private double zuzyciePaliwa;

    public Autobus(double zuzyciePaliwa, int numer, double szybkoscMaksymalna) {
        super(numer, szybkoscMaksymalna);
        this.zuzyciePaliwa = zuzyciePaliwa;
    }

    public void aktualizujZuzyciePaliwa(double zuzyciePaliwa) {
        this.zuzyciePaliwa = zuzyciePaliwa;
    }

    @Override
    public String toString() {
        return super.toString() + " zuzycie paliwa=" + zuzyciePaliwa;
    }
}
