package com.procyk.maciej.classes.trams;

public abstract class Pojazd {
    private double szybkoscMaksymalna;

    public Pojazd(double szybkoscMaksymalna) {
        this.szybkoscMaksymalna = szybkoscMaksymalna;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " szybkosc=" + szybkoscMaksymalna;
    }
}
