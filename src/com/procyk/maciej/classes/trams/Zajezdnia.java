package com.procyk.maciej.classes.trams;

import java.util.ArrayList;

public abstract class Zajezdnia {

    private final String nazwa;
    private ArrayList<PojazdKomunikacjiMiejskiej> pojazdy;

    public Zajezdnia(String nazwa) {
        this.nazwa = nazwa;
        this.pojazdy = new ArrayList<>();
    }

    public void dodajPojazd(PojazdKomunikacjiMiejskiej p) {
        p.wstawDoZajezdni(this);
        this.pojazdy.add(p);
    }

    public String nazwa() {
        return nazwa;
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder(this.getClass().getSimpleName() + " o nazwie " + nazwa + " zawiera pojazdy: ");
        for (PojazdKomunikacjiMiejskiej p : pojazdy) {
            ret.append("\n");
            ret.append(p.toString());
        }
        return ret.toString();
    }
}
