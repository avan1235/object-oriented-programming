package com.procyk.maciej.classes.trams;

import java.util.Objects;

public abstract class PojazdKomunikacjiMiejskiej extends Pojazd {

    private int numer;
    private Zajezdnia zajezdnia;

    public PojazdKomunikacjiMiejskiej(int numer, double szybkoscMaksymalna) {
        super(szybkoscMaksymalna);
        this.numer = numer;
    }

    public boolean nalezyDoZajezdni() {
        return  Objects.nonNull(zajezdnia);
    }

    public void wstawDoZajezdni(Zajezdnia z) {
        if (this.nalezyDoZajezdni())
            throw new IllegalArgumentException("Pojazd " + this.toString() + " już ma swoją zajezdnie");
        else
            this.zajezdnia = z;
    }

    @Override
    public String toString() {
        return super.toString() + " numer=" + numer + (nalezyDoZajezdni() ? " zajezdnia=" + zajezdnia.nazwa() : "");
    }
}
