package com.procyk.maciej.classes.trams;

public class Tramwaj extends PojazdKomunikacjiMiejskiej {

    public enum LiczbaWagonów {JEDEN, DWA, TRZY};

    private final LiczbaWagonów liczbaWagonów;

    public Tramwaj(LiczbaWagonów wagonów, int numer, double predkoscMaksymalna) {
        super(numer, predkoscMaksymalna);
        this.liczbaWagonów = wagonów;
    }

    @Override
    public String toString() {
        return super.toString() + " wagonow=" + (liczbaWagonów.ordinal() + 1);
    }
}
