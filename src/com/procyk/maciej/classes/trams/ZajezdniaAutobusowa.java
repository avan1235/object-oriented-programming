package com.procyk.maciej.classes.trams;

public class ZajezdniaAutobusowa extends Zajezdnia {

    public ZajezdniaAutobusowa(String nazwa) {
        super(nazwa);
    }

    @Override
    public void dodajPojazd(PojazdKomunikacjiMiejskiej p) {
        if (p instanceof Autobus)
            super.dodajPojazd(p);
        else
            throw new IllegalArgumentException("Nie można dodać " + p.getClass().getName() + " do zajezdni autobusowej");
    }
}
