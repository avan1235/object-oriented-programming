package com.procyk.maciej.classes.trams;

public class Tests {
    public static void main(String... args) {
        Zajezdnia tram = new ZajezdniaTramwajowa("Zajezdnia Tomko");
        Zajezdnia auto = new ZajezdniaAutobusowa("Zajezdnia Juli");

        tram.dodajPojazd(new Tramwaj(Tramwaj.LiczbaWagonów.DWA, 33, 67));
        tram.dodajPojazd(new Tramwaj(Tramwaj.LiczbaWagonów.TRZY, 42, 130));
        tram.dodajPojazd(new Tramwaj(Tramwaj.LiczbaWagonów.JEDEN, 2, 20));

        auto.dodajPojazd(new Autobus(9, 154, 70));
        auto.dodajPojazd(new Autobus(11, 182, 90));
        auto.dodajPojazd(new Autobus(10, 99, 45));

        System.out.println(tram);
        System.out.println(auto);
    }
}
