package com.procyk.maciej.classes.functions;

public class HammingDistance extends Metryka{

    private boolean takieSameDziedziny(Funkcja f, Funkcja g) {
        if (f.rozmiarDziedziny() != g.rozmiarDziedziny()) {
            return false;
        }
        else {
            boolean takieSame = true;
            for (int i = 0; i < f.rozmiarDziedziny(); i++) {
                if (f.elementDziedziny(i) != g.elementDziedziny(i)) {
                    takieSame = false;
                }
            }
            return takieSame;
        }
    }

    @Override
    public int odleglosc(Funkcja f, Funkcja g) {
        if (!takieSameDziedziny(f, g)) {
            return -1;
        }
        else {
            int sum = 0;
            for (int i = 0; i < f.rozmiarDziedziny(); i++) {
                sum += Math.abs(f.aplikuj(f.elementDziedziny(i)) - g.aplikuj(g.elementDziedziny(i)));
            }
            return sum;
        }
    }
}
