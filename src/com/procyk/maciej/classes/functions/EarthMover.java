package com.procyk.maciej.classes.functions;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class EarthMover extends Metryka {
    @Override
    public int odleglosc(Funkcja f, Funkcja g) {
        if (f.masa() != g.masa()) {
            return -1;
        }
        else {
            int[] dziedzinaF = f.dziedzina();
            int[] dziedzinaG = g.dziedzina();

            int i = 0, j = 0;
            int wynik = 0;

            int d = 0;
            int ost_x = 0;

            while (i < dziedzinaF.length || j < dziedzinaG.length) {
                int x;
                if (i == dziedzinaF.length) {
                    x = dziedzinaG[j];
                    j++;
                }
                else if (j == dziedzinaG.length) {
                    x = dziedzinaF[i];
                    i++;
                }
                else {
                    x = Math.min(dziedzinaF[i], dziedzinaG[j]);
                    if (dziedzinaF[i] == x) {
                        i++;
                    }
                    if (dziedzinaG[j] == x) {
                        j++;
                    }
                }
                wynik += (x - ost_x) * Math.abs(d);
                d += f.aplikuj(x) - g.aplikuj(x);
                ost_x = x;
            }
            return wynik;
        }
    }
}
