package com.procyk.maciej.classes.functions;

public abstract class Funkcja {
    private int masa;
    private int rozmiarDziedziny;
    private boolean maMase;

    public Funkcja(int rozmiarDziedziny) {
        this.rozmiarDziedziny = rozmiarDziedziny;
        maMase = false;
    }

    public int masa() {
        if(!maMase) {
            int suma = 0;
            for (int i = 0; i < rozmiarDziedziny; i++) {
                suma += aplikuj(elementDziedziny(i));
            }
            masa = suma;
            maMase = true;
        }
        return masa;
    }

    public int rozmiarDziedziny() {
        return rozmiarDziedziny;
    }

    public abstract int elementDziedziny(int i);
    public abstract int aplikuj(int x);

    public int[] dziedzina() {
        int[] d = new int[rozmiarDziedziny];
        for (int i = 0; i < rozmiarDziedziny; i++) {
            d[i] = elementDziedziny(i);
        }
        return d;
    }
}
