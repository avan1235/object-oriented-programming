package com.procyk.maciej.classes.functions;

public class FunkcjaTablicowa extends Funkcja {
    private int[] dziedzina;
    private int[] wartosci;
    private int ostatniaWaroscIndeks = -1;


    public FunkcjaTablicowa(int[] d, int[] w) {
        super(d.length);
        dziedzina = d;
        wartosci = w;
    }

    public FunkcjaTablicowa(Funkcja f) {
        super(f.rozmiarDziedziny());
        dziedzina = new int[f.rozmiarDziedziny()];
        wartosci = new int[f.rozmiarDziedziny()];
        for (int i = 0; i < f.rozmiarDziedziny(); i++) {
            dziedzina[i] = f.elementDziedziny(i);
            wartosci[i] = f.aplikuj(dziedzina[i]);
        }
    }

    private int aplikujLog(int x) {
        int p = 0, k = dziedzina.length-1;

        while (p <= k) {
            int s = (p + k) / 2;
            if (dziedzina[s] > x) {
                k = s - 1;
            }
            else if (dziedzina[s] < x) {
                p = s + 1;
            }
            else {
                ostatniaWaroscIndeks = s;
                return wartosci[s];
            }
        }

        throw new IllegalArgumentException("Nie jest w dziedzinie");
    }

    @Override
    public int aplikuj(int x) {
        if (ostatniaWaroscIndeks > 0 && ostatniaWaroscIndeks < dziedzina.length-1) {
            if (dziedzina[ostatniaWaroscIndeks-1] == x) {
                ostatniaWaroscIndeks--;
                return wartosci[ostatniaWaroscIndeks];
            }
            else if (dziedzina[ostatniaWaroscIndeks+1] == x) {
                ostatniaWaroscIndeks++;
                return wartosci[ostatniaWaroscIndeks];
            }
            else {
                return aplikujLog(x);
            }
        }
        else if (ostatniaWaroscIndeks == 0 && dziedzina[ostatniaWaroscIndeks+1] == x) {
            ostatniaWaroscIndeks++;
            return wartosci[ostatniaWaroscIndeks];
        }
        else if (ostatniaWaroscIndeks == dziedzina.length-1 && dziedzina[ostatniaWaroscIndeks-1] == x) {
            ostatniaWaroscIndeks--;
            return wartosci[ostatniaWaroscIndeks];
        }
        else {
            return aplikujLog(x);
        }
    }

    @Override
    public int elementDziedziny(int i) {
        return dziedzina[i];
    }
}
