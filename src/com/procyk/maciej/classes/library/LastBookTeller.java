package com.procyk.maciej.classes.library;

public class LastBookTeller extends Reader {

    public LastBookTeller(Interest... interests) {
        super(interests);
    }

    @Override
    public String tellAboutReading() {
        if (lastReadBookInfo == null)
            return NO_BOOKS_MESSAGE;
        else {

            return "The infos about my last read book are:\n" +
                    lastReadBookInfo.toString() + "\n" +
                    String.join("\n", readBooksRememberedThings.get(lastReadBookInfo));
        }
    }
}
