package com.procyk.maciej.classes.library;

public class CountWords implements Interest {
    private String word;

    public CountWords(String word) {
        this.word = word;
    }

    @Override
    public String rememberBook(Book book) {
        return "Word \"" + this.word + "\" counted in book " + book.countWordOccurrences(this.word) + " times.";
    }
}
