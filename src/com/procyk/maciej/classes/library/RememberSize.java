package com.procyk.maciej.classes.library;

public class RememberSize implements Interest{

    @Override
    public String rememberBook(Book book) {
        return "Book has size of " + book.lengthOfBook() + " words.";
    }
}
