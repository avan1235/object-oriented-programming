package com.procyk.maciej.classes.library;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Library {
    private List<Book> libraryBooks;
    private int limit;

    private static final Random generator = new Random();

    public Library(List<? extends Book> books, int limit) {
        this.libraryBooks = new ArrayList<>(books);
        this.limit = limit;
    }

    public void daySimulation(List<? extends Reader> todayReaders) {
        ArrayDeque<Reader> readers = new ArrayDeque<>(todayReaders);
        List<Reader> speakers = new ArrayList<>();
        readers.forEach(Reader::startNewDay);

        int n = 0;
        while (n < limit) {
            try {
                Reader reader = readers.pollFirst();
                if (reader == null)
                    break;

                Reader.Decision decision = reader.makeDecision();
                returnBookToLibrary(reader.returnLastBook());

                if (decision == Reader.Decision.TAKE_BOOK) {
                    reader.takeNextBook(giveBook());
                    readers.addLast(reader);
                }
                else {
                    speakers.add(reader);
                }
            } catch (RuntimeException exc) {
                break;
            }
            n++;
        }

        todayReaders.forEach(this::forceToReturnBook);
        speakers.forEach(this::tellReaderStory);
    }

    private Book giveBook() {
        return libraryBooks.remove(generator.nextInt(libraryBooks.size()));
    }

    private void tellReaderStory(Reader reader) {
        System.out.println(reader.tellAboutReading() + "\n");
    }

    private void returnBookToLibrary(Book book) {
        if (book != null)
            this.libraryBooks.add(book);
    }

    private void forceToReturnBook(Reader reader) {
        returnBookToLibrary(reader.returnLastBook());
    }
}
