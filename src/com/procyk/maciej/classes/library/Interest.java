package com.procyk.maciej.classes.library;

public interface Interest {
    String rememberBook(Book book);
}
