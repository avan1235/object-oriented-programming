package com.procyk.maciej.classes.library;

import java.util.Arrays;

public class TestLibrary {
    public static void main(String... args) {
        Library library = new Library(Arrays.asList(
                new Book("W pustyni i w puszczy", 2020, "Sienkiewicz", "Dawno dawno temu w pustyni i w puszczy"),
                new Book("QueVadis", 1990, "Henryk", "Podróże są fajne"),
                new Book("Mały Książe", 1942, "Exupery", "Najważniejsze jest niewidoczne dla oczu"),
                new Book("Felix", 2005, "Rafał", "Budował roboty"),
                new Book("Net i Nika", 2001, "Kosik", "Była sobie kiedyś para")), 7);

        library.daySimulation(Arrays.asList(
                new EveryBookTeller(new RememberSize(), new RememberSomeWord(2)),
                new LastBookTeller(new CountWords("dawno")),
                new WithTitleTeller(new RememberSize())
        ));
    }
}
