package com.procyk.maciej.classes.library;

import java.util.Objects;

public class BookInfo {
    private String author;
    private Integer year;
    private String title;

    public BookInfo(String title, Integer year, String author) {
        this.title = title;
        this.year = year;
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        if (!(o instanceof BookInfo)) return false;
        else {
            BookInfo that = (BookInfo) o;
            return compareMaybeNull(this.author, that.author) && compareMaybeNull(this.title, that.title);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(author) * 13 + Objects.hashCode(title);
    }

    @Override
    public String toString() {
        return "Book info:" +
               (this.author != null ? " author=" + this.author : "") +
               (this.year != null ? " year=" + this.year : "") +
               (this.title != null ? " title=" + this.title : "");
    }

    private boolean compareMaybeNull(Object o1, Object o2) {
        return ((o1 != null && o1.equals(o2)) || (o1 == null && o2 == null));
    }

    public String getAuthor() {
        return author;
    }

    public Integer getYear() {
        return year;
    }

    public String getTitle() {
        return title;
    }
}
