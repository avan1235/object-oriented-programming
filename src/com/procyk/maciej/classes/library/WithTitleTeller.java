package com.procyk.maciej.classes.library;

public class WithTitleTeller extends Reader {

    public WithTitleTeller(Interest... interests) {
        super(interests);
    }

    @Override
    public String tellAboutReading() {
        if (readBooksRememberedThings.size() == 0)
            return NO_BOOKS_MESSAGE;
        else {
            StringBuilder res = new StringBuilder();
            for (BookInfo readBook : readBooksRememberedThings.keySet()) {
                if (readBook.getTitle() != null) {
                    res.append("The infos about my book are:\n");
                    res.append(readBook.toString()).append("\n");
                    res.append(String.join("\n", readBooksRememberedThings.get(readBook)));
                }
            }
            return res.toString();
        }
    }
}
