package com.procyk.maciej.classes.library;

import java.util.Objects;

public class Book {


    private BookInfo bookInfo;
    private String content;

    public Book(String title, int year, String author, String content) {
        assert title != null;
        assert author != null;
        assert content != null;

        this.bookInfo = new BookInfo(title, year, author);
        this.content = content;
    }

    public Book(BookInfo bookInfo, String content) {
        assert bookInfo != null;
        assert  content != null;

        this.bookInfo = bookInfo;
        this.content = content;
    }

    public String getContent() {
        return this.content;
    }

    public BookInfo getFullInfo() {
        return new BookInfo(this.bookInfo.getTitle(), this.bookInfo.getYear(), this.bookInfo.getAuthor());
    }
    public int lengthOfBook() {
        return content.replaceAll("[^ ]", "").length() + 1;
    }

    public int countWordOccurrences(String word) {
        return content.toLowerCase().split(word.toLowerCase(), -1).length - 1;
    }

    public String getNthWord(int n) {
        String[] words = this.content.split(" ");
        return words[n % words.length];
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        if (!(o instanceof Book)) return false;
        else  {
            Book that = (Book) o;
            return that.content.equals(this.content) && that.bookInfo.equals(this.bookInfo);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.content) * 47 + this.bookInfo.hashCode();
    }
}
