package com.procyk.maciej.classes.library;

import java.util.*;

public abstract class Reader {

    protected BookInfo lastReadBookInfo;
    private Book toReturnBook;

    private static final Random generator = new Random();
    protected static final String NO_BOOKS_MESSAGE = "I haven't read anything yet";

    protected Map<BookInfo, List<String>> readBooksRememberedThings;
    private List<Interest> interestedIn;


    public Reader(Interest... interestedIn) {
        readBooksRememberedThings = new HashMap<>();
        this.interestedIn = Arrays.asList(interestedIn);
        toReturnBook = null;
    }

    public void startNewDay() {
        toReturnBook = null;
    }

    /**
     * Reads the given book and tries to remember its info in characteristic way
     * @param toRead book to be read by this reader
     */
    private void readBook(Book toRead) {
        if (!wasRead(toRead)) {
            BookInfo rememberedBook = getRandomPartOfBookInfo(toRead);
            List<String> rememberedThings = new ArrayList<>();
            for (Interest i : interestedIn) {
                rememberedThings.add(i.rememberBook(toRead));
            }
            readBooksRememberedThings.put(rememberedBook, rememberedThings);
            lastReadBookInfo = rememberedBook;
        }
    }

    public Book returnLastBook() {
        Book book = toReturnBook;
        toReturnBook = null;
        return book;
    }

    public enum Decision {TAKE_BOOK, GO_HOME};

    public void takeNextBook(Book nextBook) {
        toReturnBook = nextBook;
        readBook(nextBook);
    }

    public Decision makeDecision() {
        if (toReturnBook == null)
            return Decision.TAKE_BOOK;

        return generator.nextBoolean() ? Decision.TAKE_BOOK : Decision.GO_HOME;
    }

    public abstract String tellAboutReading();

    private BookInfo getRandomPartOfBookInfo(Book book) {
        BookInfo bookInfo = book.getFullInfo();
        return new BookInfo(generator.nextBoolean() ? bookInfo.getTitle() : null,
                            generator.nextBoolean() ? bookInfo.getYear() : null,
                            generator.nextBoolean() ? bookInfo.getAuthor() : null);
    }

    private boolean wasRead(Book book) {
        return readBooksRememberedThings.keySet().contains(book.getFullInfo());
    }
}
