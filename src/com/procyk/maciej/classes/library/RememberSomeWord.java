package com.procyk.maciej.classes.library;

public class RememberSomeWord implements Interest {
    private int which;

    public RememberSomeWord(int which) {
        this.which = which;
    }

    @Override
    public String rememberBook(Book book) {
        return "Remembered " + which + ". word from book is \"" + book.getNthWord(which) + "\".";
    }
}
