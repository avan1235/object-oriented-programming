package com.procyk.maciej.classes.bank;

public class CompanyNotes extends Notes {
    private int id;

    public CompanyNotes(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
