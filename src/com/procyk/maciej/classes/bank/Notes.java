package com.procyk.maciej.classes.bank;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.stream.Collectors;

public class Notes implements InvestmentSource {

    private List<NoteNode> years;

    public Notes() {
        this.years = new ArrayList<>();
    }

    public int numberOfNotes() {
        return this.years.size();
    }

    public void addNewNote(int price) {
        if (this.years.size() == 0)
            this.years.add(new NoteNode(price, 0));
        else {
            int lastPrice = this.years.get(this.years.size() - 1).getPrice();
            double change = (double) price / (double) lastPrice;
            double percent = (change - 1.) * 100.;
            this.years.add(new NoteNode(price, percent));
        }
    }

    public boolean hasOnlyPositive() {
        return this.years.stream().map(NoteNode::getPercent).filter(i -> i >= 0.).count() == this.years.size();
    }

    public double lastPercent() {
        return this.years.get(this.years.size() - 1).getPercent();
    }

    public int averageForYears(int years) {
        if (years > this.years.size())
            throw new IllegalStateException("Cannot ask before this notes started being at market");
        int sum = 0;
        int startIndex = this.years.size() - years;
        while (startIndex < this.years.size()) {
            sum += this.years.get(startIndex++).getPercent();
        }
        return (int) ((double) sum / (double) years);
    }

    @Override
    public int getMoneyState(int startYear, int startMoney) {
        int years = new GregorianCalendar().get(Calendar.YEAR) - startYear;
        if (years > this.years.size())
            throw new IllegalStateException("Cannot ask before this notes started being at market");
        int startIndex = this.years.size() - years;
        while (startIndex < this.years.size()) {
            startMoney = (int) ((double) startMoney * (1. + this.years.get(startIndex++).getPercent() / 100.));
        }
        return startMoney;
    }
}
