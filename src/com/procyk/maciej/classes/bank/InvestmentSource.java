package com.procyk.maciej.classes.bank;

public interface InvestmentSource {
    int getMoneyState(int startYear, int startMoney);
}
