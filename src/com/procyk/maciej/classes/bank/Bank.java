package com.procyk.maciej.classes.bank;

import java.util.*;

public class Bank {

    private int currentLocateId;
    private double K;
    private int bankFunds;
    private Map<Integer, Locate> locatesRegister;

    public Bank(double K, int bankFunds) {
        this.bankFunds = bankFunds;
        this.K = K;
        this.currentLocateId = 0;
        this.locatesRegister = new HashMap<>();
    }

    private int getPartForBank(int money) {
        return (int) (this.K / 100. * (double) money);
    }

    public boolean endLocateBeforeTime(int id, Client client) {
        if (id < currentLocateId) {
            Locate locate = locatesRegister.get(id);
            if (locate.owner == client) {
                int sum = locate.funds.stream().mapToInt(Fund::checkMoneyState).sum();
                int bankPart = getPartForBank(sum);
                this.bankFunds += bankPart;
                client.addMoney(sum - bankPart);
                locatesRegister.remove(id);
                return true;
            }
        }
        return false;
    }

    public boolean newLocate(Client client, int clientFunds, int years) {
        if (client.hasMoney(clientFunds)) {
            this.bankFunds += client.giveMoney(clientFunds);
            List<Fund> funds = client.createFunds(clientFunds);
            int currentYear = new GregorianCalendar().get(Calendar.YEAR);
            Locate locate = new Locate(client, funds, currentYear + years);
            this.locatesRegister.put(currentLocateId, locate);
            client.addNewLocate(currentLocateId);
            this.currentLocateId++;
            return true;
        }
        return false;
    }

    private class Locate {
        private Client owner;
        private List<Fund> funds;
        private int endYear;

        public Locate(Client client, List<Fund> funds, int endYear) {
            this.owner = client;
            this.funds = funds;
            this.endYear = endYear;
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }

        @Override
        public boolean equals(Object o) {
            return super.equals(o);
        }
    }
}
