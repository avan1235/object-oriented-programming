package com.procyk.maciej.classes.bank;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

public class DebentureFund extends Fund {

    protected Debenture investIn;

    public DebentureFund(int startMoney, int years, Market market) {
        super(startMoney, years, market);
        int endYear = this.getStartYear() + years;
        Predicate<Debenture> endsBefore = debenture -> debenture.getLastYear() <= endYear;
        List<Debenture> before = this.market.debentures().stream()
                .filter(endsBefore)
                .sorted(Comparator
                        .comparingInt(Debenture::getLastYear).reversed()
                        .thenComparingDouble(Debenture::getPercent))
                .collect(Collectors.toList());
        this.investIn = before.get(0);
    }

    @Override
    public int checkMoneyState() {
        return investIn.getMoneyState(this.getStartYear(), this.startMoney);
    }
}
