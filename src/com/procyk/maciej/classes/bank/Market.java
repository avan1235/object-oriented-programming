package com.procyk.maciej.classes.bank;

import java.util.List;

public interface Market {

    List<CompanyNotes> companiesNotes();
    List<Debenture> debentures();
    Notes goldNotes();
}
