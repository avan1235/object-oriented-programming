package com.procyk.maciej.classes.bank;

import java.util.List;
import java.util.stream.Collectors;

public class GoodCompaniesFund extends DebentureFund {
    private List<CompanyNotes> notes;
    public GoodCompaniesFund(int startMoney, int years, Market market) {
        super(startMoney, years, market);
        this.notes = market.companiesNotes().stream().filter(Notes::hasOnlyPositive).collect(Collectors.toList());
    }

    @Override
    public int checkMoneyState() {
        if (notes.size() > 0) {
            int everyMoney = this.startMoney / this.notes.size();
            return this.notes.stream().map(note -> note.getMoneyState(this.getStartYear(), everyMoney)).reduce(Integer::sum).get();
        } else {
            return super.checkMoneyState();
        }
    }
}
