package com.procyk.maciej.classes.bank;

import java.util.Comparator;
import java.util.stream.Collectors;

public class RiskFund extends Fund {

    private CompanyNotes companyNotes;

    public RiskFund(int startMoney, int years, Market market) {
        super(startMoney, years, market);
        companyNotes = market.companiesNotes().stream().sorted(Comparator.comparingDouble(Notes::lastPercent).reversed()).limit(1).collect(Collectors.toList()).get(0);
    }

    @Override
    public int checkMoneyState() {
        return companyNotes.getMoneyState(this.getStartYear(), this.startMoney);
    }
}
