package com.procyk.maciej.classes.bank;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Debenture implements InvestmentSource {
    private int id;
    private double percent;
    private int lastYear;

    public Debenture(int id, double percent, int lastYear) {
        this.id = id;
        this.percent = percent;
        this.lastYear = lastYear;
    }

    public int getId() {
        return id;
    }

    public double getPercent() {
        return percent;
    }

    public int getLastYear() {
        return lastYear;
    }

    @Override
    public int getMoneyState(int startYear, int startMoney) {
        int years = new GregorianCalendar().get(Calendar.YEAR) - startYear;
        return (int) (((double) startMoney) * Math.pow(1. + (percent / 100.), years));
    }
}
