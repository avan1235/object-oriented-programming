package com.procyk.maciej.classes.bank;

import java.util.Calendar;
import java.util.GregorianCalendar;

public abstract class Fund {
    protected int startMoney;
    protected Market market;
    private int startYear;
    private int years;

    public Fund(int startMoney, int years, Market market) {
        this.startMoney = startMoney;
        this.years = years;
        this.market = market;
        this.startYear = new GregorianCalendar().get(Calendar.YEAR);
    }

    public abstract int checkMoneyState();

    public int getStartYear() {
        return startYear;
    }
}
