package com.procyk.maciej.classes.bank;

public class NoteNode{
    private int price;
    private double percent;

    public NoteNode(int price, double percent) {
        this.price = price;
        this.percent = percent;
    }

    public int getPrice() {
        return price;
    }

    public double getPercent() {
        return percent;
    }
}
