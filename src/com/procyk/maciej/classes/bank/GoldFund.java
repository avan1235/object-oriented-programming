package com.procyk.maciej.classes.bank;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class GoldFund extends Fund {

    public GoldFund(int money, int years, Market market) {
        super(money, years, market);
    }

    @Override
    public int checkMoneyState() {
        return this.market.goldNotes().getMoneyState(this.getStartYear(), this.startMoney);
    }
}
