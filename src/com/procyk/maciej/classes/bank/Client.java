package com.procyk.maciej.classes.bank;

import java.util.ArrayList;
import java.util.List;

public abstract class Client {
    private  int money;
    private List<Integer> idLocated;

    public Client(int money) {
        this.money = money;
        idLocated = new ArrayList<>();
    }

    public void addMoney(int money) {
        this.money += money;
    }

    public boolean hasMoney(int money) {
        return this.money >= money;
    }

    public int giveMoney(int money) {
        if (this.hasMoney(money)) {
            this.money -= money;
            return money;
        } else {
            return 0;
        }
    }

    public void addNewLocate(int id) {
        idLocated.add(id);
    }

    public abstract List<Fund> createFunds(int money);

}
