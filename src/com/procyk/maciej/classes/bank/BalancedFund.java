package com.procyk.maciej.classes.bank;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.IntPredicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class BalancedFund extends DebentureFund {

    private int debentureMoney;
    private int everyCompanyMoney;
    private List<CompanyNotes> companyNotes;

    public BalancedFund(int startMoney, int years, Market market) {
        super(startMoney, years, market);
        Predicate<CompanyNotes> hasEnoughYears = companyNotes -> companyNotes.numberOfNotes() - years >= 0;
        List<CompanyNotes> notes = this.market.companiesNotes().stream().filter(hasEnoughYears).sorted(Comparator.comparingInt(company -> company.averageForYears(years))).collect(Collectors.toList());
        if (notes.size() == 0) {
            this.debentureMoney = startMoney;
        } else {
            this.companyNotes = notes.stream().filter(note -> note.averageForYears(years) == notes.get(notes.size()-1).averageForYears(years)).limit(3).collect(Collectors.toList());
            this.everyCompanyMoney = startMoney / (2 * this.companyNotes.size());
            this.debentureMoney = startMoney - (this.companyNotes.size() * everyCompanyMoney);
        }

    }

    @Override
    public int checkMoneyState() {
        try {
            return this.investIn.getMoneyState(this.getStartYear(), debentureMoney) + companyNotes.stream().map(companyNotes1 -> companyNotes1.getMoneyState(this.getStartYear(), this.everyCompanyMoney)).reduce(Integer::sum).get();
        } catch (NoSuchElementException exc) {
            return this.investIn.getMoneyState(this.getStartYear(), debentureMoney);
        }
    }
}
