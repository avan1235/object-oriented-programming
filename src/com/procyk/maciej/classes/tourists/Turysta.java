package com.procyk.maciej.classes.tourists;

public abstract class Turysta {
    private Miasto mieszka;
    protected Miasto znajdujeSię;

    public Turysta(Miasto mieszka) {
        this.mieszka = mieszka;
        this.znajdujeSię = mieszka;
    }

    public void wrocDoDomu() {
        znajdujeSię = mieszka;
    }

    public abstract void zwiedzKolejneMiasto();
}
