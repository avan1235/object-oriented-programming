package com.procyk.maciej.classes.tourists;

import java.util.*;
import java.util.stream.Collectors;

public class Miasto {
    private String nazwa;
    private int rokZalozenia;
    private int liczbaMieszkancow;

    private ArrayList<Droga> sasiednie;

    public Miasto(String nazwa, int rokZalozenia, int liczbaMieszkancow) {
        this.nazwa = nazwa;
        this.rokZalozenia = rokZalozenia;
        this.liczbaMieszkancow = liczbaMieszkancow;
        this.sasiednie = new ArrayList<>();
    }

    public void dodajSasiednie(Droga drogaZMiasta) {
        sasiednie.add(drogaZMiasta);
    }

    public String nazwa() {
        return nazwa;
    }

    public int rokZalozenia() {
        return rokZalozenia;
    }

    public int liczbaMieszkancow() {
        return liczbaMieszkancow;
    }

    public Miasto losoweSasiednie() {
        return sasiednie.get(new Random().nextInt(sasiednie.size())).dokad();
    }

    public Miasto dowolneSasiednieNajodleglejsze() {
        Collections.sort(sasiednie, Comparator.comparingInt(Droga::odleglosc).reversed());
        return sasiednie.get(0).dokad();
    }

    public ArrayList<Miasto> sasiednieMiasta() {
        ArrayList<Miasto> sasiednieMiasta = new ArrayList<>();
        sasiednie.forEach(droga -> sasiednieMiasta.add(droga.dokad()));

        return sasiednie.stream().map(Droga::dokad).collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Miasto miasto = (Miasto) o;
        return rokZalozenia == miasto.rokZalozenia &&
                liczbaMieszkancow == miasto.liczbaMieszkancow &&
                nazwa.equals(miasto.nazwa) &&
                sasiednie.equals(miasto.sasiednie);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nazwa, rokZalozenia, liczbaMieszkancow, sasiednie);
    }
}
