package com.procyk.maciej.classes.tourists;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class TurystaCiekawy extends Turysta{
    private ArrayList<Miasto> odwiedzone;

    public TurystaCiekawy(Miasto mieszka) {
        super(mieszka);
    }

    private boolean odwiedzil(Miasto m) {
        return odwiedzone.stream().anyMatch(miasto -> miasto.equals(m));
    }

    @Override
    public void zwiedzKolejneMiasto() {
        ArrayList<Miasto> nieodwiedzone = znajdujeSię.sasiednieMiasta().stream().filter(miasto -> odwiedzil(miasto)).collect(Collectors.toCollection(ArrayList::new));
        if (nieodwiedzone.size() > 0) {
            znajdujeSię = nieodwiedzone.get(0);
        }
        else {
            znajdujeSię = znajdujeSię.dowolneSasiednieNajodleglejsze();
        }
    }
}
