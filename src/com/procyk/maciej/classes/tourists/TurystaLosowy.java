package com.procyk.maciej.classes.tourists;

public class TurystaLosowy extends Turysta {

    public TurystaLosowy(Miasto mieszka) {
        super(mieszka);
    }

    @Override
    public void zwiedzKolejneMiasto() {
        znajdujeSię = znajdujeSię.losoweSasiednie();
    }
}
