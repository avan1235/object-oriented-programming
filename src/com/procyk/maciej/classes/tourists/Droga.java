package com.procyk.maciej.classes.tourists;

public class Droga {
    private int odleglosc;
    private Miasto dokad;

    public Droga(int odleglosc, Miasto dokad) {
        this.odleglosc = odleglosc;
        this.dokad = dokad;
    }

    public int odleglosc() {
        return odleglosc;
    }

    public Miasto dokad() {
        return dokad;
    }
}
