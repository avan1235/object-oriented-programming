package com.procyk.maciej.classes.tourists;

public class Skoncentrowany extends Turysta {
    private Miasto ulubioneMiasto;

    public Skoncentrowany(Miasto mieszka, Miasto ulubioneMiasto) {
        super(mieszka);
        this.ulubioneMiasto = ulubioneMiasto;
    }

    @Override
    public void zwiedzKolejneMiasto() {
        if (!znajdujeSię.equals(ulubioneMiasto)) {
            if (znajdujeSię.sasiednieMiasta().stream().anyMatch(miasto -> miasto.equals(ulubioneMiasto))) {
                znajdujeSię = ulubioneMiasto;
            }
            else {
                znajdujeSię = znajdujeSię.losoweSasiednie();
            }
        }
    }
}
