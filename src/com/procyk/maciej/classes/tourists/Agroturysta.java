package com.procyk.maciej.classes.tourists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Agroturysta extends Turysta {
    public Agroturysta(Miasto mieszka) {
        super(mieszka);
    }

    @Override
    public void zwiedzKolejneMiasto() {
        ArrayList<Miasto> sasiednie = znajdujeSię.sasiednieMiasta();
        Collections.sort(sasiednie, Comparator.comparingInt(Miasto::liczbaMieszkancow).thenComparingInt(Miasto::rokZalozenia));
        znajdujeSię = sasiednie.get(0);
    }
}
