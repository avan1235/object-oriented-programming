package com.procyk.maciej.classes.tourists;

import java.util.ArrayList;

public class Majowka {
    private final static int DNI_TYGODNIA = 7;

    public void przeprowadzMajowke(Turysta[] turysci, ArrayList<Miasto> miasta, int[][] odleglosci) {

        assert (miasta.size() > 0);
        assert (miasta.size() == odleglosci.length);
        assert (odleglosci.length == odleglosci[0].length);

        for (int x = 0; x < odleglosci.length; x++) {
            for (int y = 0; y < odleglosci[0].length; y++) {
                if (x != y && odleglosci[x][y] > 0) {
                    miasta.get(x).dodajSasiednie(new Droga(odleglosci[x][y], miasta.get(y)));
                }
            }
        }

        for (int i = 0; i < DNI_TYGODNIA; i++) {
            for (Turysta t : turysci) {
                t.zwiedzKolejneMiasto();
            }
        }

        for (Turysta t : turysci) {
            t.wrocDoDomu();
        }
    }
}
