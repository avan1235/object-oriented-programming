package com.procyk.maciej.classes.polynomials;

public class TestPolynomials {
    public static void main(String... args) {
        Polynomial p1 = new Polynomial(new int[]{0,3}, new int[]{-7,1});
        Polynomial p2 = new Polynomial(new int[]{7,0,0,1});

        System.out.println(p1);
        System.out.println(p2);

        System.out.println();

        System.out.println(p1.add(p2));
        System.out.println(p2.add(p1));

        System.out.println();

        System.out.println(p1.substract(p2));
        System.out.println(p2.substract(p1));

        System.out.println();

        System.out.println(p1.multiplay(p2));
        System.out.println(p2.multiplay(p1));

        System.out.println();

        Polynomial p0 = new Polynomial(new int[]{0,0,0,0});

        System.out.println(p0);
        System.out.println(p0.multiplay(p1));

        System.out.println();

        Polynomial p3 = new Polynomial(new int[]{1,0,3,5});
        Polynomial p4 = new Polynomial(new int[]{-1,0,-3,-5});

        System.out.println(p3);
        System.out.println(p4);
        System.out.println(p3.add(p4));
        System.out.println(p3.multiplay(p4));
    }
}
