package com.procyk.maciej.classes.polynomials;

import java.util.Arrays;

public class Polynomial {
    private static final String STANDARD_BEGIN = "P(x) = ";

    private int[] coeff;

    public Polynomial(int[] coeff) {
        int deg = coeff.length;
        this.coeff = new int[deg];

        int newDeg;
        for (newDeg = coeff.length - 1; coeff[newDeg] == 0 && newDeg > 0; newDeg--);
        newDeg++;

        for (int i = 0; i < newDeg; i++) {
            this.coeff[i] = coeff[i];
        }
    }

    public Polynomial(int[] deg, int[] coeff) {
        assert (deg.length == coeff.length);

        int maxDeg = deg[deg.length - 1] + 1;
        this.coeff = new int[maxDeg];

        for (int i = 0; i < coeff.length; i++) {
            this.coeff[deg[i]] = coeff[i];
        }
    }

    public int getCoeff(int i) {
        if (i < this.getDeg()) {
            return this.coeff[i];
        }
        else {
            return 0;
        }
    }

    public int getDeg() {
        return this.coeff.length;
    }

    public Polynomial add(Polynomial p) {
        int maxL = Math.max(p.getDeg(), this.getDeg());

        int[] newCoeff = new int[maxL];

        int i;
        for (i = 0; i < Math.min(p.getDeg(), this.getDeg()); i++) {
            newCoeff[i] = p.getCoeff(i) + this.getCoeff(i);
        }

        Polynomial newP = (p.getDeg() > this.getDeg() ? p : this);

        for (; i < maxL; i++) {
            newCoeff[i] += newP.getCoeff(i);
        }

        return new Polynomial(newCoeff);
    }

    public Polynomial multiplay(Polynomial p) {

        int[] newCoeff = new int[this.getDeg() + p.getDeg()];

        for (int mainI = 0; mainI < this.getDeg() + p.getDeg(); mainI++) {
            for (int pi = 0; pi <= mainI; pi++) {
                newCoeff[mainI] += this.getCoeff(mainI - pi) * p.getCoeff(pi);
            }
        }

        return new Polynomial(newCoeff);

    }

    private Polynomial minusCoeff() {
        int[] newCoeff = new int[this.getDeg()];

        for (int i = 0; i < this.getDeg(); i++) {
            newCoeff[i] = (-1) * this.getCoeff(i);
        }

        return new Polynomial(newCoeff);
    }

    public Polynomial substract(Polynomial p) {
        return this.add(p.minusCoeff());
    }

    private boolean isZero() {
        for (int i = 0; i < this.getDeg(); i++) {
            if (this.getCoeff(i) != 0) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        String retStr = STANDARD_BEGIN;

        if (this.coeff.length > 0) {
            if (this.coeff[0] != 0) {
                retStr += this.coeff[0] + " + ";
            }

            for (int i = 1; i < this.coeff.length; i++) {
                if (this.coeff[i] != 0) {
                    retStr += (this.coeff[i] == 1 ? "" : this.coeff[i]) + "x" + (i > 1 ? "^" + i : "") + " + ";
                }
            }
        }

        if (this.isZero()) {
            return STANDARD_BEGIN + "0";
        }

        return retStr.substring(0, retStr.lastIndexOf(" + "));
    }
}
