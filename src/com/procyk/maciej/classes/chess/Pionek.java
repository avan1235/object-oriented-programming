package com.procyk.maciej.classes.chess;

import javax.sql.PooledConnection;
import java.util.ArrayList;

public class Pionek extends Bierka {

    public Pionek(boolean kolor, int x) {
        super(kolor, x, kolor == Bierka.BIALA ? 1 : Plansza.ROZMIAR_Y - 2);
    }

    @Override
    public String znak() {
        return kolor == Bierka.BIALA ? "♙" : "♟";
    }

    @Override
    public ArrayList<Polozenie> zestawMozliwychPolozen(Plansza aktualnaPlansza) {
        ArrayList<Polozenie> mozliweRuchy = new ArrayList<>();
        if (kolor == Bierka.BIALA) {
            noweDozwolonePolozeniaDodajDoListy(mozliweRuchy, aktualnaPlansza, 1, Polozenie.GORA_JEDEN);
            if (polozenie.y() == 1) {
                noweDozwolonePolozeniaDodajDoListy(mozliweRuchy, aktualnaPlansza, 1, Polozenie.GORA_DWA);
            }
            if (Plansza.miesciSieNaPlanszy(polozenie.nowePrzesunieteO(Polozenie.GORA_PRAWO))
                && !aktualnaPlansza.czyWolnePole(polozenie.nowePrzesunieteO(Polozenie.GORA_PRAWO))
                && aktualnaPlansza.kolorNaPolu(polozenie.nowePrzesunieteO(Polozenie.GORA_PRAWO)) != this.kolor) {
                mozliweRuchy.add(polozenie.nowePrzesunieteO(Polozenie.GORA_PRAWO));
            }
            if (Plansza.miesciSieNaPlanszy(polozenie.nowePrzesunieteO(Polozenie.GORA_LEWO))
                    && !aktualnaPlansza.czyWolnePole(polozenie.nowePrzesunieteO(Polozenie.GORA_LEWO))
                    && aktualnaPlansza.kolorNaPolu(polozenie.nowePrzesunieteO(Polozenie.GORA_LEWO)) != this.kolor) {
                mozliweRuchy.add(polozenie.nowePrzesunieteO(Polozenie.GORA_LEWO));
            }
        }
        else {
            noweDozwolonePolozeniaDodajDoListy(mozliweRuchy, aktualnaPlansza, 1, Polozenie.DOL_JEDEN);
            if (polozenie.y() == 6) {
                noweDozwolonePolozeniaDodajDoListy(mozliweRuchy, aktualnaPlansza, 1, Polozenie.DOL_DWA);
            }
            if (Plansza.miesciSieNaPlanszy(polozenie.nowePrzesunieteO(Polozenie.DOL_PRAWO))
                    && !aktualnaPlansza.czyWolnePole(polozenie.nowePrzesunieteO(Polozenie.DOL_PRAWO))
                    && aktualnaPlansza.kolorNaPolu(polozenie.nowePrzesunieteO(Polozenie.DOL_PRAWO)) != this.kolor) {
                mozliweRuchy.add(polozenie.nowePrzesunieteO(Polozenie.DOL_PRAWO));
            }
            if (Plansza.miesciSieNaPlanszy(polozenie.nowePrzesunieteO(Polozenie.DOL_LEWO))
                    && !aktualnaPlansza.czyWolnePole(polozenie.nowePrzesunieteO(Polozenie.DOL_LEWO))
                    && aktualnaPlansza.kolorNaPolu(polozenie.nowePrzesunieteO(Polozenie.DOL_LEWO)) != this.kolor) {
                mozliweRuchy.add(polozenie.nowePrzesunieteO(Polozenie.DOL_LEWO));
            }
        }
        return mozliweRuchy;
    }
}
