package com.procyk.maciej.classes.chess;

import java.util.ArrayList;
import java.util.Objects;

public abstract class Bierka {
    public static final boolean BIALA = false;
    public static final boolean CZARNA = true;

    protected final boolean kolor;
    protected Polozenie polozenie;

    public Bierka(boolean kolor, int x, int y) {
        this.kolor = kolor;
        this.polozenie = new Polozenie(x, y);
    }

    public abstract String znak();

    public abstract ArrayList<Polozenie> zestawMozliwychPolozen(Plansza aktualnaPlansza);

    public Polozenie polozenie() {
        return polozenie;
    }

    public boolean kolor() {
        return kolor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        else return false;
    }

    private void noweDozwolonePolozeniaDodajDoListy(ArrayList<Polozenie> mozliweRuchy, Plansza aktualnaPlansza, boolean czyOgraniczacLiczbeRuchow, int ograniczenie, Polozenie... kierunki) {
        for (Polozenie kierunek : kierunki) {
            int i = 1;
            while ((!czyOgraniczacLiczbeRuchow || i <= ograniczenie)
                    && Plansza.miesciSieNaPlanszy(polozenie.nowePrzesunieteOWielokrotnosc(kierunek, i))
                    && aktualnaPlansza.czyWolnePole(polozenie.nowePrzesunieteOWielokrotnosc(kierunek, i))) {
                mozliweRuchy.add(polozenie.nowePrzesunieteOWielokrotnosc(kierunek, i));
                i++;
            }
            if ((!czyOgraniczacLiczbeRuchow || i <= ograniczenie)
                    && Plansza.miesciSieNaPlanszy(polozenie.nowePrzesunieteOWielokrotnosc(kierunek, i))
                    && !aktualnaPlansza.czyWolnePole(polozenie.nowePrzesunieteOWielokrotnosc(kierunek, i))
                    && aktualnaPlansza.kolorNaPolu(polozenie.nowePrzesunieteOWielokrotnosc(kierunek, i)) != this.kolor) {
                mozliweRuchy.add(polozenie.nowePrzesunieteOWielokrotnosc(kierunek, i));
            }
        }
    }

    protected void noweDozwolonePolozeniaDodajDoListy(ArrayList<Polozenie> mozliweRuchy, Plansza aktualnaPlansza, Polozenie... kierunki) {
        noweDozwolonePolozeniaDodajDoListy(mozliweRuchy, aktualnaPlansza, false, 0, kierunki);
    }

    protected void noweDozwolonePolozeniaDodajDoListy(ArrayList<Polozenie> mozliweRuchy, Plansza aktualnaPlansza, int ograniczenie, Polozenie... kierunki) {
        noweDozwolonePolozeniaDodajDoListy(mozliweRuchy, aktualnaPlansza, true, ograniczenie, kierunki);
    }
}
