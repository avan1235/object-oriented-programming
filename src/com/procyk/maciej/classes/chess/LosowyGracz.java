package com.procyk.maciej.classes.chess;

import java.util.ArrayList;
import java.util.Random;

public class LosowyGracz extends Gracz {

    public LosowyGracz(String nazwaGracza, boolean kolor) {
        super(nazwaGracza, kolor);
    }

    protected Ruch kolejnyRuch(Plansza aktualnaPlansza) throws Ruch.BrakMozliwychRuchow {
        ArrayList<ArrayList<Polozenie>> mozliwePolozeniaBierek = new ArrayList<>();
        for (Bierka b : this.aktualnyZestawBierek()) {
            mozliwePolozeniaBierek.add(b.zestawMozliwychPolozen(aktualnaPlansza));
        }
        boolean[] czyNiezeroweMozliwosci = new boolean[mozliwePolozeniaBierek.size()];
        for (int i = 0; i < mozliwePolozeniaBierek.size(); i++) {
            czyNiezeroweMozliwosci[i] = (mozliwePolozeniaBierek.get(i).size() > 0);
        }
        boolean jakiesNiepuste = false;
        for (boolean mozliwosc : czyNiezeroweMozliwosci) {
            jakiesNiepuste = jakiesNiepuste || mozliwosc;
        }

        if (!jakiesNiepuste) {
            throw new Ruch.BrakMozliwychRuchow();
        }

        int wylosowanyIndeks;
        do {
            wylosowanyIndeks = new Random().nextInt(mozliwePolozeniaBierek.size());
        } while (!czyNiezeroweMozliwosci[wylosowanyIndeks]);

        int indeksRuchu = new Random().nextInt(mozliwePolozeniaBierek.get(wylosowanyIndeks).size());
        Ruch aktualnyRuch = new Ruch(this.aktualnyZestawBierek().get(wylosowanyIndeks).polozenie(), mozliwePolozeniaBierek.get(wylosowanyIndeks).get(indeksRuchu));
        return aktualnyRuch;
    }
}
