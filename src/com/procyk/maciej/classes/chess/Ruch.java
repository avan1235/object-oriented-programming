package com.procyk.maciej.classes.chess;

public class Ruch {
    private Polozenie dawnePolozenie;
    private Polozenie nowePolozenie;

    public Ruch(Polozenie dawnePolozenie, Polozenie nowePolozenie) {
        this.dawnePolozenie = dawnePolozenie;
        this.nowePolozenie = nowePolozenie;
    }

    public Polozenie dawnePolozenie() {
        return dawnePolozenie;
    }

    public Polozenie nowePolozenie() {
        return nowePolozenie;
    }

    @Override
    public String toString() {
        return "RUCH Z " + dawnePolozenie + " NA " + nowePolozenie;
    }

    public static class BrakMozliwychRuchow extends Exception {}
}
