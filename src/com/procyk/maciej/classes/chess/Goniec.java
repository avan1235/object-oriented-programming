package com.procyk.maciej.classes.chess;

import java.util.ArrayList;

public class Goniec extends Bierka {

    public Goniec(boolean kolor, int num) {
        super(kolor, num == 0 ? 2 : Plansza.ROZMIAR_X - 3, kolor == Bierka.BIALA ? 0 : Plansza.ROZMIAR_Y - 1);
    }

    @Override
    public String znak() {
        return kolor == Bierka.BIALA ? "♗" : "♝";
    }

    @Override
    public ArrayList<Polozenie> zestawMozliwychPolozen(Plansza aktualnaPlansza) {
        ArrayList<Polozenie> mozliweRuchy = new ArrayList<>();
        noweDozwolonePolozeniaDodajDoListy(mozliweRuchy, aktualnaPlansza, Polozenie.GORA_LEWO, Polozenie.GORA_PRAWO,
                                                                          Polozenie.DOL_LEWO, Polozenie.DOL_PRAWO);
        return mozliweRuchy;
    }
}
