package com.procyk.maciej.classes.chess;

import java.util.ArrayList;

public class Wieza extends Bierka {

    public Wieza(boolean kolor, int num) {
        super(kolor, num == 0 ? 0 : Plansza.ROZMIAR_X - 1, kolor == Bierka.BIALA ? 0 : Plansza.ROZMIAR_Y - 1);
    }


    @Override
    public String znak() {
        return kolor == Bierka.BIALA ? "♖" : "♜";
    }

    @Override
    public ArrayList<Polozenie> zestawMozliwychPolozen(Plansza aktualnaPlansza) {
        ArrayList<Polozenie> mozliweRuchy = new ArrayList<>();
        noweDozwolonePolozeniaDodajDoListy(mozliweRuchy, aktualnaPlansza, Polozenie.GORA_JEDEN, Polozenie.DOL_JEDEN,
                                                                          Polozenie.LEWO_JEDEN, Polozenie.PRAWO_JEDEN);
        return mozliweRuchy;
    }
}
