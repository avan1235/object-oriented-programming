package com.procyk.maciej.classes.chess;

import java.util.Arrays;
import java.util.Objects;

public class Plansza {
    public static final int ROZMIAR_X = 8;
    public static final int ROZMIAR_Y = 8;

    private Bierka[][] bierkiPlanszy = new Bierka[ROZMIAR_X][];
    {
        for (int i = 0; i < ROZMIAR_X; i++) {
            bierkiPlanszy[i] = new Bierka[ROZMIAR_Y];
        }
    }

    public static boolean miesciSieNaPlanszy(Polozenie p) {
        return p.x() >= 0 && p.x() < ROZMIAR_X && p.y() >= 0 && p.y() < ROZMIAR_Y;
    }

    public Plansza(Gracz... gracze) {
        for (Gracz g : gracze) {
            for (Bierka bierka : g.aktualnyZestawBierek()) {
                if (Plansza.miesciSieNaPlanszy(bierka.polozenie())) {
                    bierkiPlanszy[bierka.polozenie().x()][bierka.polozenie().y()] = bierka;
                }
            }
        }
    }

    private boolean czyRuchJestBiciem(Ruch ruch) {
        int sX = ruch.dawnePolozenie().x();
        int sY = ruch.dawnePolozenie().y();
        int nX = ruch.nowePolozenie().x();
        int nY = ruch.nowePolozenie().y();
        return !Objects.isNull(bierkiPlanszy[sX][sY]) && !Objects.isNull(bierkiPlanszy[nX][nY]);
    }

    public Bierka dokonajRuchuIOpisz(Ruch ruch, StringBuilder opis) {
        int sX = ruch.dawnePolozenie().x();
        int sY = ruch.dawnePolozenie().y();
        int nX = ruch.nowePolozenie().x();
        int nY = ruch.nowePolozenie().y();
        Bierka zbitaBierka = null;
        opis.append(czyRuchJestBiciem(ruch) ? "BICIE " : "RUCH ");
        opis.append(bierkiPlanszy[sX][sY].znak());
        opis.append(" Z ");
        opis.append(ruch.dawnePolozenie());
        opis.append(" NA ");
        opis.append(ruch.nowePolozenie());
        if (czyRuchJestBiciem(ruch)) {
            opis.append(" ZBIJA ");
            opis.append(bierkiPlanszy[nX][nY].znak());
            zbitaBierka = bierkiPlanszy[nX][nY];
        }

        bierkiPlanszy[sX][sY].polozenie().ustawNa(ruch.nowePolozenie());
        bierkiPlanszy[nX][nY] = bierkiPlanszy[sX][sY];
        bierkiPlanszy[sX][sY] = null;
        return zbitaBierka;
    }

    public String toString() {
        StringBuilder opis = new StringBuilder();
        //opis.append("_| A̲ | B̲ | C̲ | D̲ | E̲ | F̲ | G̲ | H̲ |_");
        opis.append("  A B C D E F G H  ");
        opis.append(System.lineSeparator());
        for (int y = ROZMIAR_Y - 1; y >= 0; y--) {
            opis.append(y+1);
            //opis.append("̲");
            for (int x = 0; x < ROZMIAR_X; x++) {
                opis.append(" ");
                opis.append(bierkiPlanszy[x][y] != null ? bierkiPlanszy[x][y].znak() : " ");
                //opis.append("̲");
            }
            opis.append(" ");
            opis.append(y+1);
            opis.append(System.lineSeparator());
        }
        opis.append("  A B C D E F G H  ");
        opis.append(System.lineSeparator());
        return opis.toString();
    }

    public boolean czyWolnePole(Polozenie p) {
        return Objects.isNull(bierkiPlanszy[p.x()][p.y()]);
    }

    public boolean kolorNaPolu(Polozenie p) throws IllegalArgumentException {
        if (czyWolnePole(p))
            throw new IllegalArgumentException("Nie mozna pytac o kolor pustego pola");
        else
            return bierkiPlanszy[p.x()][p.y()].kolor();
    }

    public String opisMozliwychRuchow() {
        StringBuilder opis = new StringBuilder();
        for (int x = 0; x < ROZMIAR_X; x++) {
            for (int y = 0; y < ROZMIAR_Y; y++) {
                if (!Objects.isNull(bierkiPlanszy[x][y])) {
                    opis.append(bierkiPlanszy[x][y].znak());
                    opis.append(" ");
                    opis.append(bierkiPlanszy[x][y].polozenie());
                    opis.append(" ");
                    opis.append(Arrays.toString(bierkiPlanszy[x][y].zestawMozliwychPolozen(this).toArray()));
                    opis.append(System.lineSeparator());
                }
            }
        }
        return opis.toString();
    }
}
