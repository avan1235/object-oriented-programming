package com.procyk.maciej.classes.chess;

import java.io.IOException;
import java.util.Objects;

public class SzachyGra {
    private static final int ILE_TUR = 50;

    public static void main(String... args) throws IOException {
        Gracz bialy = new LosowyGracz("Heniek", Bierka.BIALA);
        Gracz czarny = new LosowyGracz("Stefek", Bierka.CZARNA);
        Plansza plansza = new Plansza(czarny, bialy);

        przeprowadzRozgrywke(plansza, bialy, czarny);
    }

    public static void przeprowadzRozgrywke(Plansza plansza, Gracz... gracze) throws IOException{
        assert gracze.length == 2;

        clearScreen();
        printMessage("NACISNIJ ENTER BY KONTYNUOWAC");
        waitForKeyPress();

        clearScreen();
        printMessage("SZACHOWNICA");
        System.out.println(plansza);
        System.out.println(gracze[0]);
        System.out.println(gracze[1]);

        int numerRuchu = 0;
        boolean koniecGry = false;
        int numerZwyciezcy = -1;
        StringBuilder opis;

        while (!koniecGry) {
            waitForKeyPress();
            clearScreen();
            Gracz aktualnyGracz = gracze[numerRuchu % 2];
            Gracz przeciwnik = gracze[(numerRuchu + 1) % 2];
            int numerTury = numerRuchu / 2 + 1;

            printMessage("TURA " + numerTury + " | RUCH WYKONUJE " + (aktualnyGracz.kolor() == Bierka.BIALA ? "BIAŁY" : "CZARNY"));
            try {
                opis = new StringBuilder();
                Ruch zadanyRuch = aktualnyGracz.kolejnyRuch(plansza);
                Bierka usuwanaBierka = plansza.dokonajRuchuIOpisz(zadanyRuch, opis);
                System.out.println(plansza);
                System.out.println(opis);
                if (!Objects.isNull(usuwanaBierka)) {
                    if (usuwanaBierka instanceof Krol) {
                        System.out.println("KRÓL ZBITY! KONIEC GRY");
                        numerZwyciezcy = numerRuchu % 2;
                        break;
                    }
                    przeciwnik.usunBierke(usuwanaBierka);
                }
            } catch (Ruch.BrakMozliwychRuchow e) {
                koniecGry = true;
                System.out.println("BRAK MOŻLIWYCH RUCHÓW");
                numerZwyciezcy = (numerRuchu + 1) % 2;
            }
            numerRuchu++;
            koniecGry = (numerTury == ILE_TUR && numerRuchu % 2 == 1);
        }


        if (numerZwyciezcy == -1) {
            printMessage("REMIS");
        }
        else {
            printMessage("ZWYCIEZCA:", gracze[numerZwyciezcy].toString());
        }
        System.out.println("KONIEC GRY");
        printSeparator();
    }

    private static void printSeparator() {
        System.out.println("------------------------------");
    }

    private static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    private static void printMessage(String... args) {
        printSeparator();
        for (String arg : args) {
            System.out.println(arg);
        }
        printSeparator();
    }

    private static void waitForKeyPress() throws IOException {
        System.in.read();
    }
}
