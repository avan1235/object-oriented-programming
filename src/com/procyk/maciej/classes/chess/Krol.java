package com.procyk.maciej.classes.chess;

import java.util.ArrayList;

public class Krol extends Bierka {

    public static final ArrayList<Polozenie> MOZLIWE_RUCHY = new ArrayList<>();

    static {
        MOZLIWE_RUCHY.add(Polozenie.GORA_JEDEN);
        MOZLIWE_RUCHY.add(Polozenie.GORA_PRAWO);
        MOZLIWE_RUCHY.add(Polozenie.GORA_LEWO);
        MOZLIWE_RUCHY.add(Polozenie.DOL_JEDEN);
        MOZLIWE_RUCHY.add(Polozenie.DOL_PRAWO);
        MOZLIWE_RUCHY.add(Polozenie.DOL_LEWO);
        MOZLIWE_RUCHY.add(Polozenie.LEWO_JEDEN);
        MOZLIWE_RUCHY.add(Polozenie.PRAWO_JEDEN);
    }

    public Krol(boolean kolor) {
        super(kolor, Plansza.ROZMIAR_X - 4, kolor == Bierka.BIALA ? 0 : Plansza.ROZMIAR_Y - 1);
    }

    @Override
    public String znak() {
        return kolor == Bierka.BIALA ? "♔" : "♚";
    }

    @Override
    public ArrayList<Polozenie> zestawMozliwychPolozen(Plansza aktualnaPlansza) {
        ArrayList<Polozenie> mozliweRuchy = new ArrayList<>();
        noweDozwolonePolozeniaDodajDoListy(mozliweRuchy, aktualnaPlansza, 1, Polozenie.GORA_JEDEN, Polozenie.DOL_JEDEN,
                Polozenie.LEWO_JEDEN, Polozenie.PRAWO_JEDEN, Polozenie.GORA_LEWO, Polozenie.GORA_PRAWO, Polozenie.DOL_LEWO, Polozenie.DOL_PRAWO);
        return mozliweRuchy;
    }
}
