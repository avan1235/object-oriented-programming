package com.procyk.maciej.classes.chess;

import java.util.ArrayList;

public abstract class Gracz {

    private final String nazwaGracza;
    private boolean kolor;
    private ArrayList<Bierka> zestawBierek;

    public Gracz(String nazwaGracza, boolean kolor) {
        this.nazwaGracza = nazwaGracza;
        this.kolor = kolor;
        this.zestawBierek = new ArrayList<>();

        for (int i = 0; i < Plansza.ROZMIAR_X; i++) {
            zestawBierek.add(new Pionek(this.kolor, i));
        }
        for (int i = 0; i < 2; i++) {
            zestawBierek.add(new Wieza(this.kolor, i));
            zestawBierek.add(new Goniec(this.kolor, i));
            zestawBierek.add(new Skoczek(this.kolor, i));
        }
        zestawBierek.add(new Hetman(this.kolor));
        zestawBierek.add(new Krol(this.kolor));
    }

    public String toString() {
        return nazwaGracza + " (" + (kolor == Bierka.BIALA ? "BIAŁE" : "CZARNE") + ")";
    }

    public ArrayList<Bierka> aktualnyZestawBierek() {
        return zestawBierek;
    }

    public void usunBierke(Bierka bierka) {
        zestawBierek.remove(bierka);
    }

    public boolean kolor() {
        return this.kolor;
    }

    protected abstract Ruch kolejnyRuch(Plansza aktualnaPlansza) throws Ruch.BrakMozliwychRuchow;
}
