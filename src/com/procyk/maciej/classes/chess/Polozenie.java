package com.procyk.maciej.classes.chess;

public class Polozenie {

    public static final Polozenie GORA_JEDEN = new Polozenie(0, 1);
    public static final Polozenie GORA_DWA = new Polozenie(0, 2);
    public static final Polozenie DOL_JEDEN = new Polozenie(0, -1);
    public static final Polozenie DOL_DWA = new Polozenie(0, -2);

    public static final Polozenie PRAWO_JEDEN = new Polozenie(1, 0);
    public static final Polozenie LEWO_JEDEN = new Polozenie(-1, 0);

    public static final Polozenie GORA_PRAWO = new Polozenie(1, 1);
    public static final Polozenie GORA_LEWO = new Polozenie(-1, 1);
    public static final Polozenie DOL_PRAWO = new Polozenie(1, -1);
    public static final Polozenie DOL_LEWO = new Polozenie(-1, -1);

    private int x;
    private int y;

    public Polozenie(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int x() {
        return this.x;
    }

    public int y() {
        return this.y;
    }

    public void zmienXO(int x) {
        this.x += x;
    }

    public void zmienYO(int y) {
        this.y += y;
    }

    public void zmienO(Polozenie p) {
        this.zmienXO(p.x());
        this.zmienYO(p.y());
    }

    public Polozenie nowePrzesunieteO(Polozenie p) {
        Polozenie nowe = new Polozenie(this.x, this.y);
        nowe.zmienO(p);
        return nowe;
    }

    public void ustawNa(Polozenie p) {
        this.x = p.x();
        this.y = p.y();
    }

    public Polozenie nowePrzesunieteOWielokrotnosc(Polozenie p, int w) {
        Polozenie nowe = this.kopia();
        for (int i = 0; i < w; i++) {
            nowe.zmienO(p);
        }
        return nowe;
    }

    public Polozenie nowePrzesunieteO(int x, int y) {
        Polozenie nowe = new Polozenie(this.x, this.y);
        nowe.zmienXO(x);
        nowe.zmienYO(y);
        return nowe;
    }

    public Polozenie kopia() {
        return new Polozenie(this.x, this.y);
    }

    @Override
    public String toString() {
        return Character.toString((char) (65 + x)) + Character.toString((char) (49 + y));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Polozenie)) return false;
        final Polozenie p = (Polozenie) obj;
        return p.x() == this.x && p.y() == this.y;

    }
}
