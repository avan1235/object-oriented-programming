package com.procyk.maciej.classes.chess;

import java.util.ArrayList;

public class Skoczek extends Bierka {

    private static final ArrayList<Polozenie> MOZLIWE_RUCHY = new ArrayList<>();

    static {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k ++) {
                    Polozenie nowyRuch = new Polozenie(0,0);
                    int x = (i == 0 ? 1 : 2);
                    int y = (i == 0 ? 2 : 1);
                    nowyRuch.zmienXO(j == 0 ? x : -x);
                    nowyRuch.zmienYO(k == 0 ? y : -y);
                    MOZLIWE_RUCHY.add(nowyRuch);
                }
            }
        }
    }

    public Skoczek(boolean kolor, int num) {
        super(kolor, num == 0 ? 1 : Plansza.ROZMIAR_X - 2, kolor == Bierka.BIALA ? 0 : Plansza.ROZMIAR_Y - 1);
    }

    @Override
    public String znak() {
        return kolor == Bierka.BIALA ? "♘" : "♞";
    }

    @Override
    public ArrayList<Polozenie> zestawMozliwychPolozen(Plansza aktualnaPlansza) {
        ArrayList<Polozenie> mozliweRuchy = new ArrayList<>();
        for (Polozenie ruch : MOZLIWE_RUCHY) {
            Polozenie nowePolozenie = polozenie.nowePrzesunieteO(ruch);
            if (Plansza.miesciSieNaPlanszy(nowePolozenie)
                && (aktualnaPlansza.czyWolnePole(nowePolozenie) || aktualnaPlansza.kolorNaPolu(nowePolozenie) != this.kolor)) {
                mozliweRuchy.add(nowePolozenie);
            }
        }
        return mozliweRuchy;
    }
}
