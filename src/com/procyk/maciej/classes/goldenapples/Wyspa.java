package procyk.maciej.goldapples;

import java.util.*;

public class Wyspa {
    private ArrayList<Jablko> jablonie;

    public Wyspa(int liczbaJabloni) {
        jablonie = new ArrayList<>(liczbaJabloni);
        for (int i = 0; i < liczbaJabloni; i++) {
            jablonie.add(new Jablko());
        }

        Collections.sort(jablonie, Comparator.comparingInt(j -> j.wartosc()));
    }

    public Jablko kupOdWyspy(int posiadaneSrodki) {
        for (int i = jablonie.size() - 1; i > -1; i--) {
            if (jablonie.get(i).wartosc() <= posiadaneSrodki) {
                return jablonie.remove(i);
            }
        }
        return null;
    }
}
