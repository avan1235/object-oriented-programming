package procyk.maciej.goldapples;

import java.util.ArrayList;

public class Symulacja {
    private static final int DNI_TYGODNIA = 7;

    public int[] przeprowadzTydzien(int[] liczbyStatkow, ArrayList<ArrayList<ArrayList<Wyspa>>> odwiedzane) {
        int[] zyski = new int[liczbyStatkow.length];
        ArrayList<Firma> firmy = new ArrayList<>();

        for (int i = 0; i < odwiedzane.size(); i++) {
            firmy.add(new Firma(odwiedzane.get(i)));
        }

        for (int i = 0; i < DNI_TYGODNIA; i++) {
            for (Firma f : firmy) {
                f.symulujDzienFirmy();
            }
        }

        for (int i = 0; i < zyski.length; i++) {
            zyski[i] = firmy.get(i).pestkiFirmy();
        }

        return zyski;
    }
}
