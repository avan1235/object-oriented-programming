package procyk.maciej.goldapples;

import java.util.ArrayList;
import java.util.Arrays;

public class Tests {

    public static void main(String... args) {
        Wyspa w1 = new Wyspa(3);
        Wyspa w2 = new Wyspa(8);
        Wyspa w3 = new Wyspa(5);
        Wyspa w4 = new Wyspa(9);
        Wyspa w5 = new Wyspa(2);
        Wyspa w6 = new Wyspa(10);
        Wyspa w7 = new Wyspa(1);
        Wyspa w8 = new Wyspa(33);
        Wyspa w9 = new Wyspa(4);
        Wyspa w10 = new Wyspa(7);

        ArrayList<Wyspa> s1f1 = new ArrayList<>();
        s1f1.add(w1);
        s1f1.add(w3);
        s1f1.add(w7);

        ArrayList<Wyspa> s2f1 = new ArrayList<>();
        s2f1.add(w2);
        s2f1.add(w5);

        ArrayList<Wyspa> s1f2 = new ArrayList<>();
        s1f2.add(w4);
        s1f2.add(w9);

        ArrayList<Wyspa> s1f3 = new ArrayList<>();
        s1f3.add(w4);
        s1f3.add(w6);

        ArrayList<Wyspa> s2f3 = new ArrayList<>();
        s2f3.add(w1);
        s2f3.add(w7);
        s2f3.add(w10);

        ArrayList<Wyspa> s3f3 = new ArrayList<>();
        s3f3.add(w8);

        ArrayList<ArrayList<Wyspa>> firma1 = new ArrayList<>();
        firma1.add(s1f1);
        firma1.add(s2f1);

        ArrayList<ArrayList<Wyspa>> firma2 = new ArrayList<>();
        firma2.add(s1f2);

        ArrayList<ArrayList<Wyspa>> firma3 = new ArrayList<>();
        firma3.add(s1f3);
        firma3.add(s2f3);
        firma3.add(s3f3);


        ArrayList<ArrayList<ArrayList<Wyspa>>> odwiedz = new ArrayList<>();
        odwiedz.add(firma1);
        odwiedz.add(firma2);
        odwiedz.add(firma3);

        Symulacja s = new Symulacja();

        int[] zyski = s.przeprowadzTydzien(new int[]{2,1,3}, odwiedz);
        System.out.println(Arrays.toString(zyski));
    }

}
