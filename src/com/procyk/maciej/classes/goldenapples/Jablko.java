package procyk.maciej.goldapples;

import java.util.Random;

public class Jablko {
    private int liczbaPestek;
    private static final int CENA_JEDNEJ_PESTKI = 40;

    public Jablko() {
        liczbaPestek = new Random().nextInt(5) + 2;
    }

    public int wartosc() {
        return CENA_JEDNEJ_PESTKI * liczbaPestek;
    }

    public int liczbaPestek() {
        return liczbaPestek;
    }
}
