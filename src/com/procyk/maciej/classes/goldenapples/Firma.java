package procyk.maciej.goldapples;

import java.util.ArrayList;

public class Firma {
    private ArrayList<Statek> statki;

    public Firma(ArrayList<ArrayList<Wyspa>> odwiedzane) {
        statki = new ArrayList<>();
        for (int i = 0; i < odwiedzane.size(); i++) {
            statki.add(new Statek(odwiedzane.get(i)));
        }
    }

    public void symulujDzienFirmy() {
        for (Statek s : statki) {
            s.symulujDzien();
        }
    }

    public int pestkiFirmy() {
        int suma = 0;
        for (Statek s : statki) {
            suma += s.sumaPestek();
        }
        return suma;
    }
}
