package procyk.maciej.goldapples;


import java.util.ArrayList;
import java.util.Objects;

public class Statek {
    private static final int INIT_PIENIADZE = 1000;
    private int pieniadze;
    private ArrayList<Jablko> kupione;
    private ArrayList<Wyspa> odwiedzane;

    public Statek(ArrayList<Wyspa> odwiedzane) {
        this.odwiedzane = odwiedzane;
        pieniadze = INIT_PIENIADZE;
        kupione = new ArrayList<>();
    }

    public void symulujDzien() {
        if (odwiedzane.size() > 0) {
            Jablko j = odwiedzane.get(0).kupOdWyspy(pieniadze);
            if (!Objects.isNull(j)) {
                pieniadze -= j.wartosc();
                kupione.add(j);
            }
        }
    }

    public int sumaPestek() {
        int suma = 0;
        for (Jablko j : kupione) {
            suma += j.liczbaPestek();
        }
        return suma;
    }
}
