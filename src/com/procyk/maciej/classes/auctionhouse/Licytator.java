package com.procyk.maciej.classes.auctionhouse;

public class Licytator {
    private void przesunNaKoniec(Uczestnik[] u, int index) {
        Uczestnik temp = u[index];

        for (int i = index; i < u.length - 1; i++) {
            u[i] = u[i+1];
        }

        u[u.length - 1] = temp;
    }

    public Uczestnik[] przeprowadz(Uczestnik[] uczestnicy, Przedmiot[] przedmioty) {
        Uczestnik[] ktoKupil = new Uczestnik[przedmioty.length];

        for (int i = 0; i < przedmioty.length; i++) {
            boolean czySprzedany = false;

            for (int j = 0; j < uczestnicy.length && !czySprzedany; j++) {
                if (uczestnicy[j].czyChceKupic(przedmioty[i]) && uczestnicy[j].czyGoStac(przedmioty[i])) {
                    uczestnicy[j].kupPrzedmiot(przedmioty[i]);
                    ktoKupil[i] = uczestnicy[j];
                }
            }
        }

        return ktoKupil;
    }
}
