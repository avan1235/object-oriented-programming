package com.procyk.maciej.classes.auctionhouse;

public class Historyk extends Uczestnik{
    private int poczatekOkresu;
    private int koniecOkresu;

    public Historyk(int poczatekOkresu, int koniecOkresu, int budzet, String pseudonim) {
        super(budzet, pseudonim);
        assert(koniecOkresu >= poczatekOkresu);
        this.poczatekOkresu = poczatekOkresu;
        this.koniecOkresu = koniecOkresu;
    }

    private boolean jestZOkresu(int rok) {
        return rok >= poczatekOkresu && rok <= koniecOkresu;
    }

    @Override
    public boolean czyChceKupic(Przedmiot p) {
        return jestZOkresu(p.getRokProdukcji());
    }
}
