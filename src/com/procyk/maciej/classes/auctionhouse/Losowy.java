package com.procyk.maciej.classes.auctionhouse;

import java.util.Random;

public class Losowy extends Uczestnik {

    public Losowy(int budzet,  String krajPochodzenia) {
        super(budzet, krajPochodzenia);
    }

    @Override
    public boolean czyChceKupic(Przedmiot p) {
        return new Random().nextBoolean();
    }
}
