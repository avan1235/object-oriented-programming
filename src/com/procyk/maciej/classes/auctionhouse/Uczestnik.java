package com.procyk.maciej.classes.auctionhouse;

public abstract class Uczestnik {
    private int budzet;
    private String pseudonim;

    public Uczestnik(int budzet, String pseudonim) {
        this.budzet = budzet;
        this.pseudonim = pseudonim;
    }

    public abstract boolean czyChceKupic(Przedmiot p);

    public boolean czyGoStac(Przedmiot p) {
        return budzet >= p.getCena();
    }

    public void kupPrzedmiot(Przedmiot p) {
        budzet -= p.getCena();
    }
}
