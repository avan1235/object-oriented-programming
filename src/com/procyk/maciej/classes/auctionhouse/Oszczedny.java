package com.procyk.maciej.classes.auctionhouse;

public class Oszczedny extends Uczestnik {
    private int ileZaproponowano;
    private double sredniaCenaDotychczas;

    public Oszczedny(int budzet, String krajPochodzenia) {
        super(budzet, krajPochodzenia);
        ileZaproponowano = 0;
        sredniaCenaDotychczas = 0;
    }

    @Override
    public boolean czyChceKupic(Przedmiot p) {
        if (ileZaproponowano < 1) {
            ileZaproponowano += 1;
            sredniaCenaDotychczas = p.getCena();
            return false;
        }
        else {
            boolean out;
            if (Double.compare(sredniaCenaDotychczas, p.getCena()) <= 0) {
                out = false;
            }
            else {
                out = true;
            }
            sredniaCenaDotychczas = (sredniaCenaDotychczas * ileZaproponowano + p.getCena()) / (ileZaproponowano + 1);
            ileZaproponowano++;

            return out;
        }
    }
}
