package com.procyk.maciej.classes.auctionhouse;

public class Sentymentalny extends Uczestnik {
    private String krajPochodzenia;

    public Sentymentalny(String krajPochodzenia, int budzet, String pseudonim) {
        super(budzet, krajPochodzenia);
        this.krajPochodzenia = krajPochodzenia;
    }

    @Override
    public boolean czyChceKupic(Przedmiot p) {
        return p.getKrajPochodzenia().equalsIgnoreCase(krajPochodzenia);
    }
}
