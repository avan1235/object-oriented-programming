package com.procyk.maciej.classes.vector;

public class NotEqualDimensionsException extends RuntimeException {
    private int d1, d2;

    public NotEqualDimensionsException(int d1, int d2) {
        this.d1 = d1;
        this.d2 = d2;
    }

    @Override
    public String toString() {
        return "Given dimensions are d1=" + d1 + " d2=" + d2;
    }
}
