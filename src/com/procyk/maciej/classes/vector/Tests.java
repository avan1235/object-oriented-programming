package com.procyk.maciej.classes.vector;

import java.util.ArrayList;

public class Tests {

    public static void main(String... args) {
        ArrayList<DoubleField> v1coords = new ArrayList<>();
        ArrayList<DoubleField> v2coords = new ArrayList<>();

        v1coords.add(new DoubleField(1));
        v1coords.add(new DoubleField(2));
        v1coords.add(new DoubleField(3));

        v2coords.add(new DoubleField(7));
        v2coords.add(new DoubleField(8));
        v2coords.add(new DoubleField(9));

        Vector<DoubleField> v1 = new Vector<>(v1coords, DoubleField::new);
        Vector<DoubleField> v2 = new Vector<>(v2coords, DoubleField::new);

        System.out.println(v1.add(v2));
        System.out.println(v2.add(v1));

        System.out.println(v1.multiplyByScalar(new DoubleField(5)));

        System.out.println(v1.scalarMultiply(v2));
        System.out.println(v2.scalarMultiply(v1));
    }

}
