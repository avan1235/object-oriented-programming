package com.procyk.maciej.classes.vector;

import java.util.ArrayList;
import java.util.function.Supplier;

public class Vector<T extends Field<T>> {

    private ArrayList<T> coords;
    private final Supplier<? extends T> generator;

    public Vector(ArrayList<T> coords, Supplier<? extends T> generator) {
        this.coords = coords;
        this.generator = generator;
    }

    @Override
    public String toString() {
        StringBuilder opis = new StringBuilder("Vector: ");
        for (int i = 0; i < coords.size(); i++) {
            opis.append("x_");
            opis.append(i+1);
            opis.append("=");
            opis.append(coords.get(i));
            opis.append(" ");
        }
        return opis.toString();
    }

    public Vector<T> add(Vector<T> s) throws NotEqualDimensionsException{
        if (s.coords.size() != this.coords.size())
            throw new NotEqualDimensionsException(s.coords.size(), this.coords.size());

        ArrayList<T> newCoords = new ArrayList<>(this.coords);

        for (int i = 0; i < newCoords.size(); i++) {
            newCoords.set(i, newCoords.get(i).add(s.coords.get(i)));
        }
        return new Vector<T>(newCoords, generator);
    }

    public Vector<T> multiplyByScalar(Field<T> s) {
        ArrayList<T> newCoords = new ArrayList<>(this.coords);

        for (int i = 0; i < newCoords.size(); i++) {
            newCoords.set(i, newCoords.get(i).multiply(s.getValue()));
        }
        return new Vector<T>(newCoords, generator);
    }

    public T scalarMultiply(Vector<T> s) throws NotEqualDimensionsException{
        if (s.coords.size() != this.coords.size())
            throw new NotEqualDimensionsException(s.coords.size(), this.coords.size());

        ArrayList<T> newCoords = new ArrayList<>(this.coords);
        for (int i = 0; i < newCoords.size(); i++)
            newCoords.set(i, newCoords.get(i).multiply(s.coords.get(i)));

        T result = generator.get();
        for (T coord : newCoords)
            result = result.add(coord);
        return result;
    }


}
