package com.procyk.maciej.classes.vector;

public class DoubleField implements Field<DoubleField> {
    double value;

    public DoubleField() {
        this(0);
    }

    public DoubleField(double value) {
        this.value = value;
    }

    public DoubleField add(DoubleField p) {
        return new DoubleField(p.getValue().value + this.value);
    }

    public DoubleField multiply(DoubleField s) {
        return new DoubleField(s.getValue().value * this.value);
    }

    public String toString() {
        return Double.toString(value);
    }

    public DoubleField zeroValue() {
        return new DoubleField(0);
    }

    public DoubleField getValue() {
        return new DoubleField(value);
    }
}
