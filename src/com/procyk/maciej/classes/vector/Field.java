package com.procyk.maciej.classes.vector;

public interface Field<T> {
    T add(T p);
    T multiply(T s);
    T getValue();
    String toString();
}
