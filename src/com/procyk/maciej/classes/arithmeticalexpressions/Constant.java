package com.procyk.maciej.classes.arithmeticalexpressions;

public class Constant extends Expression {
    double value;

    public Constant(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return Double.toString(value);
    }

    @Override
    public Expression derivative() {
        return new Constant(0);
    }

    @Override
    public double getValue(double x) {
        return value;
    }

    private static Constant create(double value) {
        if (Double.compare(value, 0) == 0) {
            return Zero.VALUE;
        }
        else if (Double.compare(value, 1) == 0) {
            return One.VALUE;
        }
        else {
            return new Constant(value);
        }
    }

    @Override
    public Expression addConstant(Constant constant) {
        return Constant.create(this.value + constant.value);
    }

    @Override
    public Expression multiplyConstant(Constant constant) {
        return Constant.create(this.value * constant.value);
    }

    @Override
    public Expression multiplyExpression(Expression expression) {
        if (Double.compare(this.value, One.VALUE.value) == 0) {
            return expression;
        }
        else if (Double.compare(this.value, Zero.VALUE.value) == 0) {
            return Zero.VALUE;
        }
        else {
            return expression.multiplyConstant(this);
        }
    }

    @Override
    public Expression addExpression(Expression expression) {
        if (Double.compare(this.value, Zero.VALUE.value) == 0) {
            return expression;
        }
        else  {
            return expression.addConstant(this);
        }
    }

    @Override
    public Expression multiplyReversedExpression(Expression expression) {
        return new Multiply(this, expression);
    }

    @Override
    public Expression addReversedExpression(Expression expression) {
        return new Add(this, expression);
    }
}
