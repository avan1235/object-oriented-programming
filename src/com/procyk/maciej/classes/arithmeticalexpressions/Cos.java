package com.procyk.maciej.classes.arithmeticalexpressions;

public class Cos extends OneArgumentExpression {

    public Cos(Expression expression) {
        super(expression);
    }

    @Override
    public String getName() {
        return "Cos";
    }

    @Override
    public Expression simpleDerivative() {
        return new Multiply(new Constant(-1), new Sin(expression));
    }

    @Override
    public double getValue(double x) {
        return Math.cos(expression.getValue(x));
    }
}
