package com.procyk.maciej.classes.arithmeticalexpressions;

public class Var extends Expression {

    @Override
    public double getValue(double x) {
        return x;
    }

    @Override
    public Expression derivative() {
        return new Constant(1);
    }

    @Override
    public String toString() {
        return "x";
    }
}
