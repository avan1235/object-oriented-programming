package com.procyk.maciej.classes.arithmeticalexpressions;

import java.util.Objects;

public abstract class Expression {

    public abstract String toString();

    public abstract Expression derivative();

    public abstract double getValue(double x);

    public double integral(double a, double b, int n) {
        if (b > a && n > 0) {
            double step = (b - a) / n;
            double result = 0;

            for (int i = 0; i < n; i++) {
                result += (getValue(a + (i * step)) + getValue(a + ((i + 1) * step))) / 2 * step;
            }
            return result;
        }
        else {
            throw new IllegalArgumentException("Bad parameters for integral");
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (Objects.isNull(obj)) return false;
        if (!(obj instanceof  Expression)) return false;
        if (obj instanceof Constant && this instanceof Constant &&
                (Double.compare(((Constant) obj).value, ((Constant) this).value) == 0)) return true;
        if (obj instanceof Var && this instanceof Var) return true;
        if (obj instanceof OneArgumentExpression && this instanceof OneArgumentExpression) {
            return ((OneArgumentExpression) obj).expression.equals(((OneArgumentExpression) this).expression);
        }
        if (obj instanceof TwoArgumentExpression && this instanceof TwoArgumentExpression) {
            return ((TwoArgumentExpression) obj).left.equals(((TwoArgumentExpression) this).left) &&
                    ((TwoArgumentExpression) obj).right.equals(((TwoArgumentExpression) this).right);
        }
        return false;
    }

    public static Expression simplifyAdd(Expression left, Expression right) {
        return left.addExpression(right);
    }

    public static Expression simplifyMultiply(Expression left, Expression right) {
        return left.multiplyExpression(right);
    }

    public Expression addExpression(Expression expression) {
        return expression.addReversedExpression(this);
    }

    public Expression addReversedExpression(Expression expression) {
        return new Add(expression, this);
    }

    public Expression addConstant(Constant constant) {
        return new Add(constant, this);
    }

    public Expression multiplyExpression(Expression expression) {
        return expression.multiplyReversedExpression(this);
    }

    public Expression multiplyReversedExpression(Expression expression) {
        return new Multiply(expression, this);
    }

    public Expression multiplyConstant(Constant constant) {
        return new Multiply(constant, this);
    }
}
