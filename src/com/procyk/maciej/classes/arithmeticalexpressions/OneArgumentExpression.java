package com.procyk.maciej.classes.arithmeticalexpressions;

public abstract class OneArgumentExpression extends Expression {
    protected Expression expression;

    protected OneArgumentExpression(Expression expression) {
        this.expression = expression;
    }

    public abstract String getName();

    public abstract Expression simpleDerivative();

    @Override
    public String toString() {
        return getName() + "[" + expression + "]";
    }

    @Override
    public Expression derivative() {
        return new Multiply(this.simpleDerivative(), expression.derivative());
    }
}
