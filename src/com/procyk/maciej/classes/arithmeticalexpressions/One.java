package com.procyk.maciej.classes.arithmeticalexpressions;

public class One extends Constant {
    private One() {
        super(1);
    }

    public static One VALUE = new One();

    @Override
    public Expression multiplyConstant(Constant constant) {
        return constant;
    }

    @Override
    public Expression multiplyExpression(Expression expression) {
        return expression;
    }

    @Override
    public Expression multiplyReversedExpression(Expression expression) {
        return expression;
    }
}
