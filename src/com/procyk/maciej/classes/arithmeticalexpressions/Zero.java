package com.procyk.maciej.classes.arithmeticalexpressions;

public class Zero extends Constant {
    private Zero() {
        super(0);
    }

    public static Zero VALUE = new Zero();

    @Override
    public Expression addExpression(Expression expression) {
        return expression;
    }

    @Override
    public Expression multiplyExpression(Expression expression) {
        return Zero.VALUE;
    }

    @Override
    public Expression addConstant(Constant constant) {
        return constant;
    }

    @Override
    public Expression multiplyConstant(Constant constant) {
        return Zero.VALUE;
    }

    @Override
    public Expression addReversedExpression(Expression expression) {
        return expression;
    }

    @Override
    public Expression multiplyReversedExpression(Expression expression) {
        return Zero.VALUE;
    }
}
