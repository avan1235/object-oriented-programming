package com.procyk.maciej.classes.arithmeticalexpressions;

public class Exp extends OneArgumentExpression {

    public Exp(Expression expression) {
        super(expression);
    }

    @Override
    public String getName() {
        return "Exp";
    }

    public Expression simpleDerivative() {
        return new Exp(expression);
    }

    @Override
    public double getValue(double x) {
        return Math.exp(x);
    }
}
