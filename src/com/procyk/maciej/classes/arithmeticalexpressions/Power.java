package com.procyk.maciej.classes.arithmeticalexpressions;

public class Power extends TwoArgumentExpression {

    public Power(Expression left, Expression right) {
        super(left, right);
    }

    @Override
    public String getSymbol() {
        return "^";
    }

    @Override
    public double getValue(double x) {
        return Math.exp(right.getValue(x) * Math.log(left.getValue(x)));
    }

    @Override
    public Expression derivative() {
        return new Multiply(new Power(left, new Subtract(right, new Constant(1))), new Add(new Multiply(left.derivative(), right), new Multiply(new Log(left), new Multiply(left, right.derivative()))));
    }
}
