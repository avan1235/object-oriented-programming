package com.procyk.maciej.classes.arithmeticalexpressions;

public class Sin extends OneArgumentExpression {

    public Sin(Expression expression) {
        super(expression);
    }

    @Override
    public String getName() {
        return "Sin";
    }

    public Expression simpleDerivative() {
        return new Cos(expression);
    }

    @Override
    public double getValue(double x) {
        return Math.sin(expression.getValue(x));
    }
}
