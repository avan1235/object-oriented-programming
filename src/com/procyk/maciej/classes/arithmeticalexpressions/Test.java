package com.procyk.maciej.classes.arithmeticalexpressions;

public class Test {
    public static void main(String... args) {
        Expression myExpr = new Add(new Power(new Var(), new Constant(4)), new Constant(1));
        Expression addExpr = new Add(new Add(new Var(), new Var()), new Constant(5));
        Expression multiExpr = new Multiply(new Multiply(new Constant(3), new Var()), new Var());
        Expression exceptSubExpr = new Subtract(new Constant(3), new Add(new Constant(5), new Var()));


//        System.out.println(myExpr);
//        System.out.println(myExpr.derivative().getValue(4));
//        System.out.println(addExpr);
//        System.out.println(multiExpr);
//        System.out.println(exceptSubExpr);
//
//        System.out.println(Expression.simplifyMultiply(new Var(), One.VALUE));
//        System.out.println(Expression.simplifyMultiply(Zero.VALUE, new Var()));
//        System.out.println(Expression.simplifyAdd(new Constant(5), new Constant(10)));
//
//        System.out.println(Expression.simplifyAdd(new Constant(0), Expression.simplifyMultiply(new Constant(1), new Var())));
//
//        System.out.println(Expression.simplifyAdd(Zero.VALUE, new Var()).derivative());

        Expression e = new Add(new Log(new Add(new Constant(0), new Power(new Var(), new Multiply(new Constant(1), new Var())))), new Multiply(new Constant(5), new Constant(3))).derivative();
        System.out.println(e);

    }
}
