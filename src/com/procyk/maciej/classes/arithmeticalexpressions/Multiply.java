package com.procyk.maciej.classes.arithmeticalexpressions;

public class Multiply extends TwoArgumentExpression {

    public Multiply(Expression left, Expression right) {
        super(left, right);
    }

    @Override
    public String getSymbol() {
        return "*";
    }

    @Override
    public Expression derivative() {
        return new Add(new Multiply(left, right.derivative()), new Multiply(left.derivative(), right));
    }

    @Override
    public double getValue(double x) {
        return left.getValue(x) * right.getValue(x);
    }
}
