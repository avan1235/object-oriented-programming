package com.procyk.maciej.classes.arithmeticalexpressions;

public class Log extends OneArgumentExpression {

    public Log(Expression expression) {
        super(expression);
    }

    @Override
    public String getName() {
        return "Log";
    }

    @Override
    public double getValue(double x) {
        return Math.log(expression.getValue(x));
    }

    public Expression simpleDerivative() {
        return new Divide(new Constant(1), expression);
    }
}
