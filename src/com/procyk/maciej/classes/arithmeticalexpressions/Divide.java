package com.procyk.maciej.classes.arithmeticalexpressions;

public class Divide extends TwoArgumentExpression {

    public Divide(Expression left, Expression right) {
        super(left, right);
    }

    @Override
    public String getSymbol() {
        return "/";
    }

    @Override
    public double getValue(double x) {
        return left.getValue(x) / right.getValue(x);
    }

    @Override
    public Expression derivative() {
        return new Divide(new Add(new Multiply(left.derivative(), right), new Multiply(left, right.derivative())), new Multiply(right, right));
    }
}
