package com.procyk.maciej.classes.arithmeticalexpressions;

public class Subtract extends TwoArgumentExpression {

    public Subtract(Expression left, Expression right) {
        super(left, right);
    }

    @Override
    public String getSymbol() {
        return "-";
    }

    @Override
    public Expression derivative() {
        return new Subtract(left.derivative(), right.derivative());
    }

    @Override
    public double getValue(double x) {
        return left.getValue(x) - right.getValue(x);
    }
}
