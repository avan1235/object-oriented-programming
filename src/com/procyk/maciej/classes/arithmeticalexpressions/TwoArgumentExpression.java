package com.procyk.maciej.classes.arithmeticalexpressions;

import java.util.Objects;

public abstract class TwoArgumentExpression extends Expression {
    protected Expression left;
    protected Expression right;

    protected TwoArgumentExpression(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    public abstract String getSymbol();

    @Override
    public String toString() {
        Expression simplified = null;

        if (this instanceof Add) {
            simplified = Expression.simplifyAdd(this.left, this.right);
        }
        else if (this instanceof Multiply) {
            simplified = Expression.simplifyMultiply(this.left, this.right);
        }

        if (this.equals(simplified) || Objects.isNull(simplified)) {
            String left = this.left.toString();
            String right = this.right.toString();

            if ((this.left instanceof Add || this.left instanceof Subtract) && (this instanceof Multiply || this instanceof Divide)) {
                left = "(" + left + ")";
            }

            if ((this instanceof Subtract || this instanceof Multiply || this instanceof Divide) && (this.right instanceof Add || this.right instanceof Subtract)) {
                right = "(" + right + ")";
            }

            return left + getSymbol() + right;
        }
        else {
            return simplified.toString();
        }
    }

}
