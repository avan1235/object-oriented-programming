package com.procyk.maciej.classes.graphpartition;

public class Pair<T1, T2> {
    private T1 k;
    private T2 v;

    public Pair(T1 t1, T2 t2) {
        this.k = t1;
        this.v = t2;
    }

    public T1 getKey() {
        return k;
    }

    public T2 getValue() {
        return v;
    }
}
