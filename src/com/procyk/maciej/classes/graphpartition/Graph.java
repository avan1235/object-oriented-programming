package com.procyk.maciej.classes.graphpartition;

import java.util.*;
import java.util.function.BiFunction;

public class Graph {
    private Map<Integer, Set<Integer>> vertices;

    public Graph(int numberOfVertices, ArrayList<IntsPair> edges) {
        vertices = new TreeMap<Integer, Set<Integer>>();
        for (int i = 0; i < numberOfVertices; i++)
            vertices.put(i, new TreeSet<Integer>());

        for (IntsPair p : edges) {
            vertices.get(p.getKey()).add(p.getValue());
            vertices.get(p.getValue()).add(p.getKey());
        }
    }

    public int numberOfVertices() {
        return this.vertices.keySet().size();
    }

    public boolean isAdjacentTo(int i1, int i2) {
        return vertices.get(i1).contains(i2) && vertices.get(i2).contains(i1);
    }

    public Graph complement() {
        int numberOfVertices = this.numberOfVertices();
        ArrayList<IntsPair> newEdges = new ArrayList<>();
        for (int i = 0; i < numberOfVertices; i++) {
            for (int j = 0; j < numberOfVertices; j++) {
                if (i != j && !this.isAdjacentTo(i, j)) {
                    newEdges.add(new IntsPair(i, j));
                }
            }
        }

        return new Graph(numberOfVertices, newEdges);
    }

    public int numberOfConnectedComponents() {
        Set<Integer> notVisited = new TreeSet<>(this.vertices.keySet());
        int number = 0;

        while (!notVisited.isEmpty()) {
            removeVisitedFrom(notVisited.iterator().next(), notVisited);
            number++;
        }

        return number;
    }

    private void removeVisitedFrom(int vertex, Set<Integer> notVisited) {
        int current = vertex;
        Stack<Integer> toVisitFrom = new Stack<>();
        Set<Integer> visited = new TreeSet<>();
        toVisitFrom.add(current);

        while (!toVisitFrom.isEmpty()) {
            current = toVisitFrom.pop();
            visited.add(current);

            notVisited.remove(current);

            for (int adj : this.vertices.get(current)) {
                if (!visited.contains(adj)) {
                    toVisitFrom.add(adj);
                }
            }
        }
    }
}
