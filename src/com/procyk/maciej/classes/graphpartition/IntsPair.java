package com.procyk.maciej.classes.graphpartition;

public class IntsPair extends Pair<Integer, Integer> {

    public IntsPair(int i1, int i2) {
        super(i1, i2);
    }
}
