package com.procyk.maciej.classes.graphpartition;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Tests {
    public static void main(String... args) {
        IntsPair p1 = new IntsPair(0, 1);
        IntsPair p2 = new IntsPair(0, 2);
        IntsPair p3 = new IntsPair(1, 2);
        IntsPair p4 = new IntsPair(2, 3);

        ArrayList<IntsPair> edges = new ArrayList<>();
        edges.add(p1);
        edges.add(p2);
        edges.add(p3);
        edges.add(p4);

        Graph g = new Graph(4, edges);
        System.out.println(g.complement().numberOfConnectedComponents());

        ArrayList<IntsPair> cliqueEdges = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                cliqueEdges.add(new IntsPair(i, j));
            }
        }

        Graph clique = new Graph(10, cliqueEdges);
        System.out.println(clique.complement().numberOfConnectedComponents());
    }
}
