package com.procyk.maciej.classes.fractions;

public class TestFractions {
    public static void main(String... args) {
        Ulamek jednaTrzecia = new Ulamek(1, 3);
        Ulamek trzyCzwarte = new Ulamek(3,4);
        Ulamek dwieTrzecie = new Ulamek(2,3);
        Ulamek dwa = new Ulamek(2);
        Ulamek czterdziesciTrzySiodme = new Ulamek(43,7);

        System.out.println(Ulamek.dodaj(jednaTrzecia, trzyCzwarte));
        System.out.println(Ulamek.dodaj(dwieTrzecie, jednaTrzecia));
        System.out.println(Ulamek.dodaj(dwa, dwieTrzecie));
        System.out.println(Ulamek.dodaj(3, jednaTrzecia));
        System.out.println(Ulamek.podziel(trzyCzwarte, 7));
        System.out.println(Ulamek.odejmij(jednaTrzecia, trzyCzwarte));
        System.out.println(Ulamek.pomnoz(jednaTrzecia, trzyCzwarte));
        System.out.println(Ulamek.podziel(dwieTrzecie, 4));
        System.out.println(Ulamek.odejmij(1, czterdziesciTrzySiodme));

        System.out.println(Ulamek.pomnoz(new Ulamek(-3,-4), new Ulamek(4,-7)));
    }
}
