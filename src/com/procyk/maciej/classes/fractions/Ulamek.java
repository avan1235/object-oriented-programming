package com.procyk.maciej.classes.fractions;

public class Ulamek {

    private static final String ERROR_CODE = "Nie dziel przez zero!!!";

    private int licznik;
    private int mianownik;

    public Ulamek(int licznik, int mianownik) {
        if (mianownik != 0) {
            this.licznik = licznik;
            this.mianownik = mianownik;
        }
        else{
            System.out.println(ERROR_CODE);
            this.licznik = 0;
            this.mianownik = 1;
        }
    }

    public Ulamek(int wartosc) {
        this(wartosc, 1);
    }

    private static Ulamek uprosc(Ulamek u) {
        int l = u.getLicznik();
        int m = u.getMianownik();

        int nwdWartosc = nwd(l, m);

        return new Ulamek(l / nwdWartosc, m / nwdWartosc);
    }

    public static Ulamek dodaj(Ulamek jed, Ulamek dwa) {
        int a = jed.getLicznik();
        int b = jed.getMianownik();

        int c = dwa.getLicznik();
        int d = dwa.getMianownik();

        int mianWyn = b * d;
        int licznWyn = (a * d) + (c * b);

        return uprosc(new Ulamek(licznWyn, mianWyn));
    }

    public static Ulamek dodaj(int wart, Ulamek dwa) {
        return dodaj(new Ulamek(wart), dwa);
    }

    public static Ulamek pomnoz(Ulamek jed, Ulamek dwa) {
        return uprosc(new Ulamek(jed.getLicznik()*dwa.getLicznik(),
                                 jed.getMianownik()*dwa.getMianownik()));
    }

    public static Ulamek pomnoz(int wart, Ulamek dwa) {
        return pomnoz(new Ulamek(wart), dwa);
    }

    public static Ulamek podziel(Ulamek jed, Ulamek dwa){
        if (dwa.getLicznik() == 0) {
            System.out.println(ERROR_CODE);
            return new Ulamek(0);
        }
        else {
            return pomnoz(jed, new Ulamek(dwa.getMianownik(), dwa.getLicznik()));
        }
    }

    public static Ulamek podziel(int wart, Ulamek dwa) {
        return podziel(new Ulamek(wart), dwa);
    }

    public static Ulamek podziel(Ulamek jed, int dwa) {
        return podziel(jed, new Ulamek(dwa));
    }

    public static Ulamek odejmij(Ulamek jed, Ulamek dwa) {
        return dodaj(jed, new Ulamek((-1)*dwa.getLicznik(), dwa.getMianownik()));
    }

    public static Ulamek odejmij(int wart, Ulamek dwa) {
        return odejmij(new Ulamek(wart), dwa);
    }

    public static Ulamek odejmij(Ulamek jed, int wart) {
        return odejmij(jed, new Ulamek(wart));
    }

    private static int nwd(int a, int b) {
        if (b == 0) {
            return a;
        }
        else {
            return nwd(b, a%b);
        }
    }

    public String toString() {
        String wyn = "";
        int licz = licznik;
        int mian = mianownik;

        if (licznik < 0 ^ mianownik < 0) {
            wyn += "-";

            licz = Math.abs(licznik);
            mian = Math.abs(mian);
        }

        int calk = licz/mian;
        int ulam = licz%mian;

        if (ulam == 0) {
            return wyn + Integer.toString(calk);
        }
        else {
            if (calk == 0) return wyn + licz + "/" + mian;
            else return wyn + calk + " " + ulam + "/" + mian;
        }
    }

    int getLicznik() {
        return this.licznik;
    }

    int getMianownik() {
        return this.mianownik;
    }
}
