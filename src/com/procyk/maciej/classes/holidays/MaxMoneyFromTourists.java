package com.procyk.maciej.classes.holidays;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MaxMoneyFromTourists implements Criteria {
    @Override
    public Plan optimalPlan(List<Tourist> tourists, Plan plan, List<PlanChange> availableChanges) {
        List<Plan> sortedPlans = Plan.getPossiblePlans(plan, availableChanges).stream()
                .sorted(Comparator.comparingDouble(planCur -> moneyFromTouristsWhoChoosePlan(tourists, planCur)))
                .collect(Collectors.toList());
        return sortedPlans.get(sortedPlans.size() - 1);
    }

    private double moneyFromTouristsWhoChoosePlan(List<Tourist> tourists, Plan plan) {
        Predicate<Tourist> acceptPlan = t -> t.isAGoodPlanForMe(plan);
        return tourists.stream().filter(acceptPlan).mapToDouble(Tourist::funds).sum();
    }
}
