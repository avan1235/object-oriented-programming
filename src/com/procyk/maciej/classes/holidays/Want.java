package com.procyk.maciej.classes.holidays;

public class Want extends CheckEveryPredicateNeed {
    public Want(String city) {
        super(city, () -> true);
    }
}
