package com.procyk.maciej.classes.holidays;

public class CityAdd implements PlanChange {
    private String city;

    public CityAdd(String city) {
        this.city = city;
    }

    @Override
    public Plan changedPlan(Plan plan)  throws UnsupportedOperationException {
        return plan.add(this.city);
    }
}
