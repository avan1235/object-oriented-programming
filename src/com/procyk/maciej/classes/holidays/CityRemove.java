package com.procyk.maciej.classes.holidays;

public class CityRemove implements PlanChange {

    private String city;

    public CityRemove(String city) {
        this.city = city;
    }

    @Override
    public Plan changedPlan(Plan plan) throws UnsupportedOperationException  {
        return plan.remove(this.city);
    }
}
