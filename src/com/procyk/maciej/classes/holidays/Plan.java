package com.procyk.maciej.classes.holidays;

import java.util.*;
import java.util.stream.Collectors;

public class Plan implements Iterable<String> {

    private Set<String> cities;

    public Plan(String... cities) {
        this.cities = Arrays.stream(cities).collect(Collectors.toSet());
    }

    public boolean contains(String city) {
        return this.cities.contains(city);
    }

    public Plan remove(String city) {
        if (cities.contains(city)) {
            Plan newPlan = new Plan((String[]) this.cities.toArray());
            newPlan.cities.remove(city);
            return newPlan;
        } else {
            throw new UnsupportedOperationException();
        }
    }

    public Plan add(String city) {
        if (!cities.contains(city)) {
            Plan newPlan = new Plan((String[]) this.cities.toArray());
            newPlan.cities.add(city);
            return newPlan;
        } else {
            throw new UnsupportedOperationException();
        }
    }

    public Plan change(String from, String to) {
        if (cities.contains(from) && !cities.contains(to)) {
            Plan newPlan = new Plan((String[]) this.cities.toArray());
            newPlan.cities.remove(from);
            newPlan.cities.add(to);
            return newPlan;
        } else {
            throw new UnsupportedOperationException();
        }
    }

    public Plan applyListOfChanges(List<PlanChange> changes) {
        try {
            Plan startPlan = this;
            for (int i = 0; i < changes.size(); i++)
                startPlan = changes.get(i).changedPlan(startPlan);
            return startPlan;
        } catch (UnsupportedOperationException exc) {
            return null;
        }
    }

    @Override
    public Iterator<String> iterator() {
        return cities.iterator();
    }

    public static List<Plan> getPossiblePlans(Plan startPlan, List<PlanChange> availableChanges) {
        List<List<PlanChange>> possibleChanges = new ArrayList<>();
        possibleChanges.add(new ArrayList<>());
        availableChanges.forEach(planChange -> createSingleSubsets(possibleChanges, planChange));
        for (PlanChange change1 : availableChanges) {
            for (PlanChange change2 : availableChanges) {
                ArrayList<PlanChange> pair = new ArrayList<>();
                pair.add(change1);
                pair.add(change2);
                possibleChanges.add(pair);
            }
        }
        return possibleChanges.stream()
                .map(startPlan::applyListOfChanges)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public static void createSingleSubsets(List<List<PlanChange>> possibleChanges, PlanChange planChange) {
        ArrayList<PlanChange> single = new ArrayList<>();
        single.add(planChange);
        possibleChanges.add(single);
    }
}
