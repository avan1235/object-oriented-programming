package com.procyk.maciej.classes.holidays;

public class PragmaticTourist extends Tourist {
    public PragmaticTourist(double funds, Need... needs) {
        super(funds, needs);
    }

    @Override
    public boolean isAGoodPlanForMe(Plan plan) {
        int goodNeeds = 0;
        for (Need need : this.needs) {
            if (need.isGoodEnough(plan))
                goodNeeds++;
        }
        return 2 * goodNeeds >= this.needs.size();
    }
}
