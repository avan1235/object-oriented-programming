package com.procyk.maciej.classes.holidays;

import java.util.Arrays;
import java.util.List;

public abstract class Tourist {
    protected List<Need> needs;
    private double funds;

    public Tourist(double funds, Need... needs) {
        this.needs = Arrays.asList(needs);
        this.funds = funds;
    }

    public double funds() {
        return this.funds;
    }

    public abstract boolean isAGoodPlanForMe(Plan plan);
}
