package com.procyk.maciej.classes.holidays;

public class DontWant extends CheckEveryPredicateNeed {
    public DontWant(String city) {
        super(city, () -> false);
    }
}
