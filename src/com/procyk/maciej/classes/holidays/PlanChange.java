package com.procyk.maciej.classes.holidays;

public interface PlanChange {
    Plan changedPlan(Plan plan) throws UnsupportedOperationException ;
}
