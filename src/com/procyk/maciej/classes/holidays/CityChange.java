package com.procyk.maciej.classes.holidays;

public class CityChange implements PlanChange {

    private String from;
    private String to;

    public CityChange(String from, String to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public Plan changedPlan(Plan plan) throws UnsupportedOperationException {
        return plan.change(from, to);
    }
}
