package com.procyk.maciej.classes.holidays;

import java.util.List;

public interface Criteria {
    Plan optimalPlan(List<Tourist> tourists, Plan plan, List<PlanChange> availableChanges);
}