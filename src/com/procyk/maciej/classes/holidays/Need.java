package com.procyk.maciej.classes.holidays;

public interface Need {
    boolean isGoodEnough(Plan plan);
}
