package com.procyk.maciej.classes.holidays;

import java.util.function.BooleanSupplier;

public abstract class CheckEveryPredicateNeed implements Need {
    private String city;
    private BooleanSupplier whenExistsCity;

    public CheckEveryPredicateNeed(String city, BooleanSupplier whenExistsCity) {
        this.city = city;
        this.whenExistsCity = whenExistsCity;
    }


    @Override
    public boolean isGoodEnough(Plan plan) {
        if (plan.contains(city))
            return whenExistsCity.getAsBoolean();
        return !whenExistsCity.getAsBoolean();
    }
}
