package com.procyk.maciej.classes.holidays;

import java.util.List;

public class MaxNumberOfTourists extends SortedNumberOfTourists {
    @Override
    public Plan chooseFromSorted(List<? extends Plan> plans) {
        return plans.get(plans.size() - 1);
    }
}
