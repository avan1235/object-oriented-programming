package com.procyk.maciej.classes.holidays;

public class HardTourist extends Tourist {
    public HardTourist(double funds, Need... needs) {
        super(funds, needs);
    }

    @Override
    public boolean isAGoodPlanForMe(Plan plan) {
        for (Need need : this.needs) {
            if (!need.isGoodEnough(plan))
                return false;
        }
        return true;
    }
}
