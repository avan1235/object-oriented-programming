package com.procyk.maciej.classes.holidays;

import java.util.List;

public class MinNumberOfTourists extends SortedNumberOfTourists {
    @Override
    public Plan chooseFromSorted(List<? extends Plan> plans) {
        return plans.get(0);
    }
}
