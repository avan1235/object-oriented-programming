package com.procyk.maciej.classes.holidays;

public class AnyTourist extends Tourist {
    public AnyTourist(double funds, Need... needs) {
        super(funds, needs);
    }

    @Override
    public boolean isAGoodPlanForMe(Plan plan) {
        return true;
    }
}
