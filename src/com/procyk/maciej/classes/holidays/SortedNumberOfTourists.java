package com.procyk.maciej.classes.holidays;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class SortedNumberOfTourists implements Criteria {

    @Override
    public Plan optimalPlan(List<Tourist> tourists, Plan plan, List<PlanChange> availableChanges) {
        List<Plan> sortedPlans =
                Plan.getPossiblePlans(plan, availableChanges).stream()
                .sorted(Comparator.comparingLong(planCur -> howManyChoosePlan(tourists, planCur)))
                .collect(Collectors.toList());
        return chooseFromSorted(sortedPlans);
    }

    public abstract Plan chooseFromSorted(List<? extends Plan> plans);

    private long howManyChoosePlan(List<Tourist> tourists, Plan plan) {
        Predicate<Tourist> acceptPlan = t -> t.isAGoodPlanForMe(plan);
        return tourists.stream().filter(acceptPlan).count();
    }
}
