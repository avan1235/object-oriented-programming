package com.procyk.maciej.classes.holidays;

public class NormalTourist extends Tourist {
    public NormalTourist(double funds, Need... needs) {
        super(funds, needs);
    }

    @Override
    public boolean isAGoodPlanForMe(Plan plan) {
        for (Need need : this.needs) {
            if (need.isGoodEnough(plan))
                return true;
        }
        return false;
    }
}
