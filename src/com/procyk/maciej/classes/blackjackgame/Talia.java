package com.procyk.maciej.classes.blackjackgame;

import java.util.ArrayList;
import java.util.Random;

public class Talia {
    private ArrayList<Karta> karty;

    public Talia() {
        karty = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            for (int j = 2; j <= 10; j++) {
                karty.add(new Karta(j));
            }
            karty.add(new Karta(11));
            for (int k = 0; k < 3; k++) {
                karty.add(new Karta(10));
            }
        }
        assert (karty.size() == 52);
    }

    public void tasuj() {
        for (int i = 0; i < 52; i++) {
            int j = new Random().nextInt(52-i) + i;
            Karta temp = karty.get(j);
            karty.set(j, karty.get(i));
            karty.set(i, temp);
        }
    }

    public Karta dajKarte() {
        if (karty.size() > 0) {
            return karty.remove(0);
        }
        else {
            throw new IndexOutOfBoundsException("Brak kart");
        }
    }

    public void przyjmij(ArrayList<Karta> k) {
        for (Karta karta : k) {
            karty.add(karta);
        }
    }

    public int liczbaKarta() {
        return karty.size();
    }
}
