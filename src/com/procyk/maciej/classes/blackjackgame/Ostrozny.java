package com.procyk.maciej.classes.blackjackgame;

public class Ostrozny extends Limitowy{
    public Ostrozny() {
        super(11);
    }

    @Override
    public String toString() {
        return "Ostrozny " + super.toString();
    }
}
