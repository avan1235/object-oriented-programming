package com.procyk.maciej.classes.blackjackgame;

import java.util.Random;

public class Losowy extends GraczA {
    @Override
    public boolean czyDobieram() {
        return new Random().nextBoolean();
    }

    @Override
    public String toString() {
        return "Losowy " + super.toString();
    }
}
