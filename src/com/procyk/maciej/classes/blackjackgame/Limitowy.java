package com.procyk.maciej.classes.blackjackgame;

public class Limitowy extends GraczA{
    private int limit;

    public Limitowy(int Limit) {
        this.limit = limit;
    }

    @Override
    public boolean czyDobieram() {
        return this.podajPunkty() < limit;
    }

    @Override
    public String toString() {
        return "Limitowy " + super.toString();
    }
}
