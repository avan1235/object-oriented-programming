package com.procyk.maciej.classes.blackjackgame;

public class Krupier extends Limitowy {
    public Krupier() {
        super(17);
    }

    @Override
    public String toString() {
        return "Krupier " + super.toString();
    }
}
