package com.procyk.maciej.classes.blackjackgame;

public class Karta {
    private int oczka;

    public Karta(int oczka) {
        this.oczka = oczka;
    }

    public int oczka() {
        return oczka;
    }

    @Override
    public String toString() {
        return "kartka o " + oczka + " oczkach";
    }

}
