package com.procyk.maciej.classes.blackjackgame;

import java.util.ArrayList;

public abstract class GraczA implements Gracz{
    private int sumaOczek;
    private int punkty;
    private ArrayList<Karta> karty;

    public GraczA() {
        karty = new ArrayList<>();
        punkty = 0;
        sumaOczek = 0;
    }

    public void dobieramKarte(Karta k) {
        karty.add(k);
        sumaOczek += k.oczka();
    }

    public void dodajPunkt() {
        punkty += 1;
    }

    public abstract boolean czyDobieram();

    public ArrayList<Karta> karty() {
        ArrayList<Karta> temp = karty;
        karty = new ArrayList<>();
        return temp;
    }

    public int podajSume() {
        return sumaOczek;
    }

    public int podajPunkty() {
        return punkty;
    }

    public void nowaPartia() {
        sumaOczek = 0;
    }

    public void nowaGra() {
        sumaOczek = 0;
        punkty = 0;
    }

    public String toString() {
        return "gracz o sumie punktow " + punkty;
    }
}
