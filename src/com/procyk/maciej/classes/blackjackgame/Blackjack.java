package com.procyk.maciej.classes.blackjackgame;

public class Blackjack {
    public void graj(Gracz[] gracze, int ileRozdan) {
        Talia talia = new Talia();
        int sumaZwyciezcy = -1;

        for (Gracz g : gracze) {
            g.nowaGra();
        }

        int numer = 1;

        while (ileRozdan > 0) {
            talia.tasuj();
            for (Gracz g : gracze) {
                g.nowaPartia();

                while (g.czyDobieram() && g.podajSume() <= 21) {
                    assert (talia.liczbaKarta() > 0);
                    g.dobieramKarte(talia.dajKarte());
                }

                if (g.podajSume() <= 21) {
                    sumaZwyciezcy = Math.max(sumaZwyciezcy, g.podajSume());
                }
            }

            for (Gracz g : gracze) {
                if (g.podajSume() == sumaZwyciezcy) {
                    g.dodajPunkt();
                }

                talia.przyjmij(g.karty());
            }

            System.out.println("Rozgrywka numer " + numer + ". Punkty graczy:");
            for (Gracz g : gracze) {
                System.out.println(g);
            }
            System.out.println();

            ileRozdan--;
            numer++;
        }
    }
}
