package com.procyk.maciej.classes.blackjackgame;

public class SymulacjaGry {
    public static void main(String... args) {
        Gracz[] gracze = new Gracz[5];
        gracze[0] = new Krupier();
        gracze[1] = new Ostrozny();
        gracze[2] = new Losowy();
        gracze[3] = new Spiacy();
        gracze[4] = new Limitowy(18);

        Blackjack blackjack = new Blackjack();

        blackjack.graj(gracze, 10);
    }
}
