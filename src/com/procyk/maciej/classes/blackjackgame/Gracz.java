package com.procyk.maciej.classes.blackjackgame;

import java.util.ArrayList;

interface Gracz {
    void dobieramKarte(Karta k);

    void dodajPunkt();

    boolean czyDobieram();

    ArrayList<Karta> karty();

    int podajSume();

    int podajPunkty();

    void nowaPartia();

    void nowaGra();
}
