package com.procyk.maciej.java.threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadsBasics {
    public static void main(String... args) {
        Runnable hello = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    System.out.println("Hello " + i);
                }
            }
        };

        Runnable bye = () -> {
            for (int i = 0 ; i < 1000; i++) {
                System.out.println("Byebye " + i);
            }
        };
        ExecutorService service = Executors.newCachedThreadPool();
        service.execute(hello);
        service.execute(bye);
        service.shutdown();
    }
}
