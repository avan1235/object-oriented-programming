package com.procyk.maciej.java.functional;

public class FunctionalInterfacesUsage {
    public static void main(String... args) {
        Pamiatka p = new Pamiatka();

        p.wykorzystajWartosc(wart -> {
            System.out.println("Moja wartosc przez lambde to " + wart);
        });
    }
}
