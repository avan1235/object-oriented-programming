package com.procyk.maciej.java.functional;

import java.util.Random;
import java.util.function.Consumer;

public class Pamiatka extends Przedmiot {
    private int wartosc;

    public Pamiatka() {
        wartosc = new Random().nextInt(100);
    }

    public void wykorzystajWartosc(Consumer<Integer> c) {
        c.accept(wartosc);
    }
}
