package com.procyk.maciej.java.functional;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamsTests {
    public static void main(String... args) {
        List<Przedmiot> przedmioty;
        List<Przedmiot> przedmioty2;

        przedmioty = Stream.generate(Przedmiot::new).limit(100).collect(Collectors.toList());
        przedmioty2 = Stream.generate(Przedmiot::new).limit(100).collect(Collectors.toList());

        System.out.println("Wszystkich przedmiotow jest " + przedmioty.stream().count());
        System.out.println("O cenie co najmniej 50 jest ich " + przedmioty.stream().filter(p -> p.cena() >= 50).count());
        System.out.println("Są to " + Arrays.toString(przedmioty.stream().filter(p -> p.cena() >= 50).toArray()));
        System.out.println("Najdrozszy to " + przedmioty.stream().max(Comparator.comparingInt(Przedmiot::cena)).get().toString());
        System.out.println("Najtanszy to " + przedmioty.stream().min(Comparator.comparingInt(Przedmiot::cena)).get().toString());
        System.out.println("Pierwiastki cen to " + Arrays.toString(przedmioty.stream().mapToInt(p -> (int) Math.sqrt(p.cena())).toArray()));

        System.out.println("Polaczone listy przedmiotow " + Stream.of(przedmioty, przedmioty2).flatMap(p -> p.stream()).collect(Collectors.toList()));
        System.out.println("Wsrod przedmiotow " + ((Stream.of(przedmioty, przedmioty2).flatMap(p -> p.stream()).anyMatch(p -> p.cena() == 1)) ? ("jest przedmiot") : ("nie ma przedmiotu")) + " o cenie rownej 1");

        System.out.println("Suma cen przedmiotw 1 to " + przedmioty.stream().mapToInt(p -> p.cena()).reduce(0, (acc, c) -> acc + c));
    }

    private static final Random generator = new Random();

    public List<String> randomPart(List<String> inputList, int howMany) {
        if (inputList.size() < howMany)
            throw new IllegalArgumentException();
        return inputList.stream()
                        .sorted((o1, o2) -> (o1.equals(o2) ? 0 : (generator.nextBoolean() ? -1 : 1)))
                        .limit(howMany)
                        .collect(Collectors.toList());
    }
}
