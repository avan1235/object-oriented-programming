package com.procyk.maciej.java.functional;

import java.util.Random;

public class Przedmiot {
    private int cena;

    public Przedmiot() {
        cena = new Random().nextInt(100) + 1;
    }

    public int cena() {
        return cena;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + " cena=" + cena;
    }
}
